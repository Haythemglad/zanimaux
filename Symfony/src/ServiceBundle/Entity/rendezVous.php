<?php

namespace ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use UserBundle\Entity\User;

/**
 * rendezVous
 *
 * @ORM\Table(name="rendez_vous")
 * @ORM\Entity(repositoryClass="ServiceBundle\Repository\rendezVousRepository")
 */
class rendezVous
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createDt", type="datetime")
     */
    private $createDt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updateDt", type="datetime")
     */
    private $updateDt;

    /**
     * @ORM\Column(name="valide", type="boolean")
     */
    private $valide = false;

    /**
     * @ORM\Column(name="remove", type="boolean")
     */
    private $remove = false;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="text")
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="heure", type="string")
     */
    private $heure;


    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="rendezDem")
     */
    private $demandeur;

    /**
    * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="rendezAcc")
    */
    private $accepteur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return rendezVous
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * Set createDt
     *
     * @param \DateTime $createDt
     *
     * @return rendezVous
     */
    public function setCreateDt($createDt)
    {
        $this->createDt = $createDt;

        return $this;
    }

    /**
     * Get createDt
     *
     * @return \DateTime
     */
    public function getCreateDt()
    {
        return $this->createDt;
    }

    /**
     * Set updateDt
     *
     * @param \DateTime $updateDt
     *
     * @return rendezVous
     */
    public function setUpdateDt($updateDt)
    {
        $this->updateDt = $updateDt;

        return $this;
    }

    /**
     * Get updateDt
     *
     * @return \DateTime
     */
    public function getUpdateDt()
    {
        return $this->updateDt;
    }


    /**
     * Set date
     *
     * @param string $date
     *
     * @return rendezVous
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set demandeur
     *
     * @param \UserBundle\Entity\User $demandeur
     *
     * @return rendezVous
     */
    public function setDemandeur(\UserBundle\Entity\User $demandeur = null)
    {
        $this->demandeur = $demandeur;

        return $this;
    }

    /**
     * Get demandeur
     *
     * @return \UserBundle\Entity\User
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }



    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return rendezVous
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set accepteur
     *
     * @param \UserBundle\Entity\User $accepteur
     *
     * @return rendezVous
     */
    public function setAccepteur(\UserBundle\Entity\User $accepteur = null)
    {
        $this->accepteur = $accepteur;

        return $this;
    }

    /**
     * Get accepteur
     *
     * @return \UserBundle\Entity\User
     */
    public function getAccepteur()
    {
        return $this->accepteur;
    }

    /**
     * Set heure
     *
     * @param string $heure
     *
     * @return rendezVous
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return string
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set remove
     *
     * @param boolean $remove
     *
     * @return rendezVous
     */
    public function setRemove($remove)
    {
        $this->remove = $remove;

        return $this;
    }

    /**
     * Get remove
     *
     * @return boolean
     */
    public function getRemove()
    {
        return $this->remove;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return rendezVous
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }




/*
    public function getDemandeurId()
    {
        return $this->demandeur.$this->getId();
    }
*/
}

<?php

namespace ServiceBundle\Controller;


use ServiceBundle\Entity\rendezVous;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;



class MobileController extends Controller
{


    public function AfficherrdvattnteuserAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $listeRendezVous=$em->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$id,'valide'=>false,'remove'=>false));
        $listRendezVousJson = array ();



        foreach ($listeRendezVous as $rdv) {

            $ao = $rdv->getDemandeur();
            $ao->getId();

            $oa = $rdv->getAccepteur();
            $oa->getId();

            $listRendezVousJson[] = array(

                "id" => $rdv->getId(),
                "message" => $rdv->getMessage(),
                "date" => $rdv->getDate(),
                "heure" => $rdv->getHeure(),
                "demandeur_id" => $ao->getId(),
                "demandeur_mail" => $ao->getEmail(),
                "accepteur_id" => $oa->getId(),
                "accepteur_mail" => $oa->getEmail(),
                "service" => $rdv->getService(),

            );
        }


        return new JsonResponse(array("listRendezVous" => $listRendezVousJson ));


    }

    public function AfficherrdvattntesuperuserAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $listeRendezVous=$em->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$id,'valide'=>false,'remove'=>false));
        $listRendezVousJson = array ();



        foreach ($listeRendezVous as $rdv) {

            $ao = $rdv->getDemandeur();
            $ao->getId();

            $oa = $rdv->getAccepteur();
            $oa->getId();

            $listRendezVousJson[] = array(

                "id" => $rdv->getId(),
                "message" => $rdv->getMessage(),
                "date" => $rdv->getDate(),
                "heure" => $rdv->getHeure(),
                "demandeur_id" => $ao->getId(),
                "demandeur_mail" => $ao->getEmail(),
                "accepteur_id" => $oa->getId(),
                "accepteur_mail" => $oa->getEmail(),
                "service" => $rdv->getService(),

            );
        }


        return new JsonResponse(array("listRendezVous" => $listRendezVousJson ));


    }

    public function AfficherrdvuserAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $listeRendezVous=$em->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$id,'valide'=>true,'remove'=>false));
        $listRendezVousJson = array ();
        foreach ($listeRendezVous as $rdv) {

            $ao = $rdv->getDemandeur();
            $ao->getId();

            $oa = $rdv->getAccepteur();
            $oa->getId();

            $listRendezVousJson[] = array(

                "id" => $rdv->getId(),
                "message" => $rdv->getMessage(),
                "date" => $rdv->getDate(),
                "heure" => $rdv->getHeure(),
                "demandeur_id" => $ao->getId(),
                "demandeur_mail" => $ao->getEmail(),
                "accepteur_id" => $oa->getId(),
                "accepteur_mail" => $oa->getEmail(),
                "service" => $rdv->getService(),

            );
        }


        return new JsonResponse(array("listRendezVous" => $listRendezVousJson ));


    }

    public function AfficherrdvsuperuserAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $listeRendezVous=$em->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$id,'valide'=>true,'remove'=>false));
        $listRendezVousJson = array ();
        foreach ($listeRendezVous as $rdv) {

            $ao = $rdv->getDemandeur();
            $ao->getId();

            $oa = $rdv->getAccepteur();
            $oa->getId();

            $listRendezVousJson[] = array(

                "id" => $rdv->getId(),
                "message" => $rdv->getMessage(),
                "date" => $rdv->getDate(),
                "heure" => $rdv->getHeure(),
                "demandeur_id" => $ao->getId(),
                "demandeur_mail" => $ao->getEmail(),
                "accepteur_id" => $oa->getId(),
                "accepteur_mail" => $oa->getEmail(),
                "service" => $rdv->getService(),

            );
        }


        return new JsonResponse(array("listRendezVous" => $listRendezVousJson ));


    }

    public function AfficherdresseurAction()
    {

        $em=$this->getDoctrine()->getManager();
        $listeuser=$em->getRepository('UserBundle:User')->finddresseur();
        $listeuserJson = array ();
        foreach ($listeuser as $user) {

            $listeuserJson[] = array(

                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "prenom" => $user->getPrenom(),
                "numtel" => $user->getNumtel(),
                "mail" => $user->getEmailCanonical(),
                "adresse" => $user->getHabitation(),
                "longitude" => $user->getLongitude(),
                "latitude" => $user->getLatitude(),


            );
        }


        return new JsonResponse(array("listeuser" => $listeuserJson ));


    }


    public function AfficheveterinaireAction()
    {

        $em=$this->getDoctrine()->getManager();
        $listeuser=$em->getRepository('UserBundle:User')->findveterinaire();
        $listeuserJson = array ();
        foreach ($listeuser as $user) {

            $listeuserJson[] = array(


                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "prenom" => $user->getPrenom(),
                "numtel" => $user->getNumtel(),
                "mail" => $user->getEmailCanonical(),
                "adresse" => $user->getHabitation(),
                "longitude" => $user->getLongitude(),
                "latitude" => $user->getLatitude(),


            );
        }


        return new JsonResponse(array("listeuser" => $listeuserJson ));


    }

    public function AffichegardeanimauxAction()
    {

        $em=$this->getDoctrine()->getManager();
        $listeuser=$em->getRepository('UserBundle:User')->findgardeur();
        $listeuserJson = array ();
        foreach ($listeuser as $user) {

            $listeuserJson[] = array(

                "id" => $user->getId(),
                "nom" => $user->getNom(),
                "prenom" => $user->getPrenom(),
                "numtel" => $user->getNumtel(),
                "mail" => $user->getEmailCanonical(),
                "adresse" => $user->getHabitation(),
                "longitude" => $user->getLongitude(),
                "latitude" => $user->getLatitude(),


            );
        }


        return new JsonResponse(array("listeuser" => $listeuserJson ));


    }

    public function AfficheallrdvAction()
    {

        $em=$this->getDoctrine()->getManager();
        $listeRendezVous=$em->getRepository('ServiceBundle:rendezVous')->findAll();
        $listRendezVousJson = array ();
        foreach ($listeRendezVous as $rdv) {

            $ao = $rdv->getDemandeur();
            $ao->getId();

            $oa = $rdv->getAccepteur();
            $oa->getId();

            $listRendezVousJson[] = array(

                "id" => $rdv->getId(),
                "message" => $rdv->getMessage(),
                "date" => $rdv->getDate(),
                "heure" => $rdv->getHeure(),
                "demandeur_id" => $ao->getId(),
                "accepteur_id" => $oa->getId(),
                "service" => $rdv->getService(),

            );
        }


        return new JsonResponse(array("listRendezVous" => $listRendezVousJson ));


    }




    public function ajoutrdvAction(Request $request,$id,$idd){


        $em = $this->getDoctrine()->getManager();
        $Ren = new rendezVous();


        $user = $em->getRepository('UserBundle:User')->find($id);


        $userr = $em->getRepository('UserBundle:User')->find($idd);

        $Ren->setMessage($request->get('message'));
        $Ren->setDate($request->get('date'));
        $Ren->setHeure($request->get('heure'));
        $Ren->setCreateDt(new \DateTime());
        $Ren->setUpdateDt(new \DateTime());
        $Ren->setDemandeur($user);
        $Ren->setValide($request->get('valide'));
        $Ren->setAccepteur($userr);
        $Ren->setRemove($request->get('remove'));
        $Ren->setService($request->get('service'));

        $em->persist($Ren);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Ren);
        return new JsonResponse($formatted);
    }

    public function modifierrdvAction(Request $request,$idrdv){


        $em = $this->getDoctrine()->getManager();
        $Ren = $em->getRepository('ServiceBundle:rendezVous')->find($idrdv);

        $Ren->setDate($request->get('date'));
        $Ren->setHeure($request->get('heure'));
        $em->persist($Ren);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Ren);
        return new JsonResponse($formatted);
    }

    public function annulerrdvAction(Request $request,$idrdv){


        $em = $this->getDoctrine()->getManager();
        $Ren = $em->getRepository('ServiceBundle:rendezVous')->find($idrdv);

        $Ren->setRemove($request->get('remove'));
        $em->persist($Ren);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Ren);
        return new JsonResponse($formatted);
    }

    public function validerrdvAction(Request $request,$idrdv){


        $em = $this->getDoctrine()->getManager();
        $Ren = $em->getRepository('ServiceBundle:rendezVous')->find($idrdv);

        $Ren->setValide($request->get('valide'));
        $em->persist($Ren);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Ren);
        return new JsonResponse($formatted);
    }



    public function mapAction($id){


        $m=$this->getDoctrine()->getManager();
        $map=$m->getRepository('UserBundle:User')->find($id);

        $lon=$map->getLongitude();
        $lat=$map->getLatitude();

        return $this->render('ServiceBundle:RendezVous:mapp.html.twig', array('lon'=>$lon,'lat'=>$lat));
    }





}



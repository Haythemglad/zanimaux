<?php

namespace ServiceBundle\Controller;


use blackknight467\StarRatingBundle\Form\RatingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use UserBundle\Entity\User;
use UserBundle\Form\UserForm;
use UserBundle\Form\UserType;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TemplateBundle:Default:index.html.twig');
    }

    public function RDVAction()
    {
        return $this->render('ServiceBundle:RendezVous:listerdv.html.twig');
    }



    public function RDVdressageAction(Request $request)
    {

        $m=$this->getDoctrine()->getManager();
        $dresseur=$m->getRepository('UserBundle:User')->finddresseur();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $dresseur, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        if ($request->isXmlHttpRequest())
        {
            $serializer=new  Serializer(array(new ObjectNormalizer()));

            $dresseur = $m->getRepository('UserBundle:User')->findnomDQL($request->get('nom'));
            $data = $serializer->normalize($dresseur);
            return new JsonResponse($data);

        }



        return $this->render('ServiceBundle:RendezVous:Rdvdressage.html.twig',array('m'=>$pagination));
    }




    public function RDVmedicaleAction(Request $request)
    {
        $m=$this->getDoctrine()->getManager();
        $veterinaire=$m->getRepository('UserBundle:User')->findveterinaire();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $veterinaire, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );


        if ($request->isXmlHttpRequest())
        {
            $serializer = new serializer (array (new ObjectNormalizer()));

            $data= $serializer->normalize($veterinaire);

            return new JsonResponse($data);
        }
        return $this->render('ServiceBundle:RendezVous:Rdvmedicale.html.twig',array('m'=>$pagination));
    }

    public function RDVgardeAction(Request $request)
    {
        $m=$this->getDoctrine()->getManager();
        $gardeur=$m->getRepository('UserBundle:User')->findgardeur();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $gardeur, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        if ($request->isXmlHttpRequest())
        {
            $serializer = new serializer (array (new ObjectNormalizer()));

            $data= $serializer->normalize($gardeur);

            return new JsonResponse($data);
        }
        return $this->render('ServiceBundle:RendezVous:Rdvgarde.html.twig',array('m'=>$pagination));
    }
}

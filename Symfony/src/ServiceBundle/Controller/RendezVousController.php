<?php

namespace ServiceBundle\Controller;

use UserBundle\Entity\User;
use ServiceBundle\Entity\rendezVous;
use ServiceBundle\Form\dispoType;
use ServiceBundle\Form\rendezVousType;
use ServiceBundle\ServiceBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RendezVousController extends Controller
{



    public function addRendezVousAction()
    {

        return $this->render('ServiceBundle:RendezVous:Rdv.html.twig');
    }


    public  function MakeRendezVousAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $rendezVous = new rendezVous();
        $form = $this->createForm(rendezVousType::class);
        $accepteur = $em->getRepository('UserBundle:User')->find($id);

        $accepteurroles=$accepteur->getRoles();

        $usernom = $this->getUser()->getNom();
        $userprenom = $this->getUser()->getPrenom();


        if($form->handleRequest($request)->isValid()) {

            $rendezVous = $form->getData();

            if (in_array("ROLE_DRE", $accepteurroles) == true) {
                $rendezVous->setService("Dressage");
            }

            if (in_array("ROLE_VET", $accepteurroles) == true) {
                $rendezVous->setService("Médcine");
            }

            if (in_array("ROLE_PET", $accepteurroles) == true) {
                $rendezVous->setService("Garde d'animaux");
            }


            $rendezVous->setCreateDt(new \DateTime());
            $rendezVous->setUpdateDt(new \DateTime());
            $rendezVous->setDemandeur($user);
            $rendezVous->setAccepteur($accepteur);

            $em->persist($rendezVous);
            $em->flush();

            $message = "Votre rendez-vous à été enregisté, pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Création d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn'=>'AnimauxTN'))
                ->setTo($user->getEmailCanonical())
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig',array('user'=>$user,'message'=>$message)))
            ;
            $this->get('mailer')->send($email);


            $message = "Vous avez un rendez-vous à valider avec".$usernom."  ".$userprenom.", pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Création d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn'=>'AnimauxTN'))
                ->setTo($accepteur->getEmailCanonical())
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig',array('user'=>$user,'message'=>$message)))
            ;
            $this->get('mailer')->send($email);




            return $this->redirectToRoute('rendezVous_add');

        }

        return $this->render('ServiceBundle:RendezVous:make_rendez_vous.html.twig',array('form'=>$form->createView(),'id'=>$accepteur->getId()));
    }

    public function loadCalendrierDataAction($id){

        $em=$this->getDoctrine()->getManager();
        //Liste des categories
        $listCategorieJson = array (
            array(
                "libelle" => "Apres-Midi",
                "couleur" => "#e12626",
            ),
            array(
                "libelle" => "Matin",
                "couleur" => "#e12626",
            )
        );





        //Liste des evenements
        $listeRendezVous=$em->getRepository('ServiceBundle:RendezVous')->findBy(array('accepteur'=>$id,'remove'=>false));

        $listRendezVousJson = array ();
        foreach ($listeRendezVous as $rendezVou) {
            $currentCategorie = "";
            if ($rendezVou->getCreateDt()->format('H').'h' == "08h" || $rendezVou->getCreateDt()->format('H').'h' == "09h" || $rendezVou->getCreateDt()->format('H').'h' == "10h" || $rendezVou->getCreateDt()->format('H').'h' == "011h" || $rendezVou->getCreateDt()->format('H').'h' == "12h"){
                $currentCategorie = "Matin";
            }else{
                $currentCategorie = "Apres-Midi";
            }
            $listRendezVousJson[] = array(
                "id" => $rendezVou->getId(),
                "categorie" => $currentCategorie,
                "titre" => $rendezVou->getDate()."   à    ".$rendezVou->getHeure(),
                "date" => "".strtotime($rendezVou->getCreateDt()->format('d-m-Y H:i:s'))."",
                "description" => "Cette date et heure est déja prise",
                "lien" => ""

            );
        }


        return new JsonResponse(array( 'listCategories' => $listCategorieJson ,"listRendezVous" => $listRendezVousJson ));
        return $this->render('ServiceBundle:RendezVous:make_rendez_vous.html.twig');

    }









    public function MAPAction($id)
    {
        $m=$this->getDoctrine()->getManager();
        $map=$m->getRepository('UserBundle:User')->find($id);
        //var_dump($map);
        $lon=$map->getLongitude();
        $lat=$map->getLatitude();

        return $this->render('ServiceBundle:RendezVous:map.html.twig', array('lon'=>$lon,'lat'=>$lat));
    }



    public function CheckDispoAction($date,$time,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $date = str_replace('-','/',$date);


        $exist = $em->getRepository('ServiceBundle:rendezVous')->Check($date,$time,$id);


        if ($exist)
            $message = 1;
        else
            $message = 0;

        $response = new JsonResponse();
        return $response->setData(array('message'=>$message));
    }

}

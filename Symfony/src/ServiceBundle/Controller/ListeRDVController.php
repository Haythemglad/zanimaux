<?php

namespace ServiceBundle\Controller;

use ServiceBundle\Entity\rendezVous;
use ServiceBundle\Form\rendezVousType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListeRDVController extends Controller
{

    public function AfficherRDVAction(Request $request)
    {
        $user = $this->getUser()->getRoles();
        $userid = $this->getUser()->getId();


        if (in_array("ROLE_VET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );


            return $this->render('ServiceBundle:RendezVous:listerdv.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_DRE", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );


            return $this->render('ServiceBundle:RendezVous:listerdv.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_PET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render('ServiceBundle:RendezVous:listerdv.html.twig', array('m' => $pagination));

        }

    }

    public function AfficherRDVdispoAction(Request $request)
    {
        $user = $this->getUser()->getRoles();
        $userid = $this->getUser()->getId();
        if (in_array("ROLE_VET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>false,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render('ServiceBundle:RendezVous:listerdvdispo.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_DRE", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>false,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render('ServiceBundle:RendezVous:listerdvdispo.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_PET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('accepteur'=>$userid,'valide'=>false,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render('ServiceBundle:RendezVous:listerdvdispo.html.twig', array('m' => $pagination));

        }
    }


        public function SupprimerAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $rdv=$em->getRepository('ServiceBundle:rendezVous')->find($id);
        $rdv->setRemove(true);
        $em->persist($rdv);
        $em->flush();
        $user = $this->getUser();
        $userroles = $this->getUser()->getRoles();
        $accepteurnom = $rdv->getAccepteur()->getNom();
        $accepteurprenom = $rdv->getAccepteur()->getPrenom();
        $demandeurnom = $rdv->getDemandeur()->getNom();
        $demandeurprenom = $rdv->getDemandeur()->getPrenom();
        $accepteurmail = $rdv->getAccepteur()->getEmailCanonical();
        $demandeurmail = $rdv->getDemandeur()->getEmailCanonical();

        if (in_array("ROLE_SIMPLE-USER", $userroles) == true) {


            $message = "Votre avez annuler votre rendez-vous" . $id . "avec" . $accepteurnom . " " . $accepteurprenom . " , pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Annulation d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                ->setTo($user->getEmailCanonical())
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
            $this->get('mailer')->send($email);


            $message = "Vous avez un rendez-vous qui à été annulé" . $id . "avec" . $demandeurnom . " " . $demandeurprenom . ", pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Annulation d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                ->setTo($accepteurmail)
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
            $this->get('mailer')->send($email);

        }


        if ((in_array("ROLE_DRE", $userroles) == true) || (in_array("ROLE_PET", $userroles) == true)  || (in_array("ROLE_VET", $userroles) == true)) {


            $message = "Votre avez annuler  votre rendez-vous" . $id . "avec" . $demandeurnom . " " . $demandeurprenom . " , pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Annulation d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                ->setTo($user->getEmailCanonical())
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
            $this->get('mailer')->send($email);


            $message = "Vous avez un rendez-vous qui à été annulé" . $id . "avec" . $accepteurnom . " " . $accepteurprenom . ", pour plus d'information vérifier votre compte.";
            $email = \Swift_Message::newInstance()
                ->setSubject("Annulation d'un rendez-vous")
                ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                ->setTo($demandeurmail)
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
            $this->get('mailer')->send($email);


        }


        return $this->redirectToRoute('rendezVous_add');

    }

    public function ValiderAction(Request $request,$id)
    {

        $em=$this->getDoctrine()->getManager();
        $rdv=$em->getRepository('ServiceBundle:rendezVous')->find($id);
        $user = $this->getUser();
        $demandeurnom = $rdv->getDemandeur()->getNom();
        $demandeurprenom = $rdv->getDemandeur()->getPrenom();
        $usernom = $this->getUser()->getNom();
        $userprenom = $this->getUser()->getPrenom();
        $demandeur = $rdv->getDemandeur()->getEmailCanonical();

            $rdv->setValide(true);
            $rdv->setAccepteur($user);
            $em=$this->getDoctrine()->getManager();
            $em->persist($rdv);
            $em->flush();

        $message = "Vous avez valider le rendez-vous N°".$id."avec".$demandeurnom." ".$demandeurprenom. " , pour plus d'information vérifier votre compte.";
        $email = \Swift_Message::newInstance()
            ->setSubject("Validation d'un rendez-vous")
            ->setFrom(array('test@idealconstruction.tn'=>'AnimauxTN'))
            ->setTo($user->getEmailCanonical())
            ->setCharset('UTF-8')
            ->setContentType('text/html')
            ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig',array('user'=>$user,'message'=>$message)))
        ;
        $this->get('mailer')->send($email);


        $message = "Votre rendez-vous N°".$id." à été validé avec".$usernom." ".$userprenom. ", pour plus d'information vérifier votre compte.";
        $email = \Swift_Message::newInstance()
            ->setSubject("Validation d'un rendez-vous")
            ->setFrom(array('test@idealconstruction.tn'=>'AnimauxTN'))
            ->setTo($demandeur)
            ->setCharset('UTF-8')
            ->setContentType('text/html')
            ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig',array('user'=>$user,'message'=>$message)))
        ;
        $this->get('mailer')->send($email);


        return $this->redirectToRoute('rendezVous_add');


        return $this->render('ServiceBundle:RendezVous:listerdvdispo.html.twig', array('m' => $rdv));

    }
    public function ModifierAction($id,request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $userroles = $this->getUser()->getRoles();
        $rdv = $em->getRepository('ServiceBundle:rendezVous')->find($id);
        $accepteurnom = $rdv->getAccepteur()->getNom();
        $accepteurprenom = $rdv->getAccepteur()->getPrenom();
        $demandeurnom = $rdv->getDemandeur()->getNom();
        $demandeurprenom = $rdv->getDemandeur()->getPrenom();
        $accepteurmail = $rdv->getAccepteur()->getEmailCanonical();
        $demandeurmail = $rdv->getDemandeur()->getEmailCanonical();

            $form = $this->createForm(rendezVousType::class, $rdv);
        if ($form->handleRequest($request)->isValid()) {


            $em->persist($rdv);
            $em->flush();


            if (in_array("ROLE_SIMPLE-USER", $userroles) == true) {

                $message = "Votre avez changer la date de votre rendez-vous" . $id . "avec" . $accepteurnom . " " . $accepteurprenom . " , pour plus d'information vérifier votre compte.";
                $email = \Swift_Message::newInstance()
                    ->setSubject("Modification d'un rendez-vous")
                    ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                    ->setTo($user->getEmailCanonical())
                    ->setCharset('UTF-8')
                    ->setContentType('text/html')
                    ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
                $this->get('mailer')->send($email);


                $message = "Vous avez un rendez-vous qui à été changé N°  " . $id . "  avec  " . $demandeurnom . "    " . $demandeurprenom . "  , pour plus d'information vérifier votre compte.";
                $email = \Swift_Message::newInstance()
                    ->setSubject("Modification d'un rendez-vous")
                    ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                    ->setTo($accepteurmail)
                    ->setCharset('UTF-8')
                    ->setContentType('text/html')
                    ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
                $this->get('mailer')->send($email);

            }



            if ((in_array("ROLE_DRE", $userroles) == true) || (in_array("ROLE_PET", $userroles) == true)  || (in_array("ROLE_VET", $userroles) == true)) {

                $message = "Votre avez changer la date de votre rendez-vous" . $id . "avec" . $demandeurnom . " " . $demandeurprenom . " , pour plus d'information vérifier votre compte.";
                $email = \Swift_Message::newInstance()
                    ->setSubject("Annulation d'un rendez-vous")
                    ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                    ->setTo($user->getEmailCanonical())
                    ->setCharset('UTF-8')
                    ->setContentType('text/html')
                    ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
                $this->get('mailer')->send($email);


                $message = "Vous avez un rendez-vous qui à été changé N°  " . $id . "avec" . $accepteurnom . " " . $accepteurprenom . ", pour plus d'information vérifier votre compte.";
                $email = \Swift_Message::newInstance()
                    ->setSubject("Annulation d'un rendez-vous")
                    ->setFrom(array('test@idealconstruction.tn' => 'AnimauxTN'))
                    ->setTo($demandeurmail)
                    ->setCharset('UTF-8')
                    ->setContentType('text/html')
                    ->setBody($this->render('ServiceBundle:RendezVous:mail.html.twig', array('user' => $user, 'message' => $message)));
                $this->get('mailer')->send($email);






            }


            return $this->redirectToRoute('rendezVous_add');



        }


        return $this->render('ServiceBundle:RendezVous:modifier.html.twig', array('form' => $form->createView(),'idd'=>$rdv->getAccepteur()->getId(),'id'=>$rdv->getId()));
    }


    public function AfficherRDVALLAction(Request $request)
    {

        $m=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$user,'valide'=>true,'remove'=>false));

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $rdv, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );


        return $this->render('ServiceBundle:RendezVous:listerdvuser.html.twig', array('m'=>$pagination));
        // ...

    }

    public function AfficherRDVNONAction(Request $request)
    {

        $m=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$user,'valide'=>false,'remove'=>false));


        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $rdv, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render('ServiceBundle:RendezVous:listerdvusernon.html.twig', array('m'=>$pagination));
        // ...

    }

        // ...


    public function AfficherRDVPROAction(Request $request)
    {
        $user = $this->getUser()->getRoles();
        $userid = $this->getUser()->getId();


        if (in_array("ROLE_VET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );


            return $this->render('ServiceBundle:RendezVous:listerdvpro.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_DRE", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );


            return $this->render('ServiceBundle:RendezVous:listerdvpro.html.twig', array('m' => $pagination));

        }

        if (in_array("ROLE_PET", $user) == true) {

            $m = $this->getDoctrine()->getManager();
            $rdv=$m->getRepository('ServiceBundle:rendezVous')->findBy(array('demandeur'=>$userid,'valide'=>true,'remove'=>false));

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $rdv, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render('ServiceBundle:RendezVous:listerdvpro.html.twig', array('m' => $pagination));

        }

    }

}

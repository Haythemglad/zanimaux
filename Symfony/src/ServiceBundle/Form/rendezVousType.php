<?php

namespace ServiceBundle\Form;


use ServiceBundle\Repository\rendezVousRepository;
use ServicesBundle\Entity\services;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use UserBundle\UserBundle;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
class rendezVousType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message')
            ->add('date')
            ->add('heure', ChoiceType::class, array(
                'choices'  => array(
                    '8h' => '8h',
                    '9h' => '9h',
                    '10h' => '10h',
                    '11h' => '11h',
                    '12h' => '12h',
                    '14h' => '14h',
                    '15h' => '15h',
                    '16h' => '16h',
                    '17h' => '17h',

                ),
            ))



        ;
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ServiceBundle\Entity\rendezVous'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'servicebundle_rendezvous';
    }


}

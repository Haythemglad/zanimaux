<?php
/**
 * Created by PhpStorm.
 * User: mehdi
 * Date: 10/02/2018
 * Time: 19:46
 */

namespace AdministrationBundle\Controller;


use AdministrationBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ReclamationController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdministrationBundle:Reclamation:admin_reclamation_index.html.twig');
    }


    public function loadReclamationDataTablesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $reclamationList = $em->getRepository('AdministrationBundle:Reclamation')->findAll();
        foreach ($reclamationList as $reclamation) {
            $user = $em->getRepository('UserBundle:User')->find($reclamation->getUser());
            //$category = $em->getRepository('WSBundle:Category')->find($project->getCategory());
            $reclamationListJson[] = array(
                $user->getUsername(),
                $reclamation->getCategorie(),
                $reclamation->getDateCreation()->format("Y/m/d H:i:s"),
//                $reclamation->getEstValide(),
                $reclamation->getTitre(),
                $reclamation->getDescription(),
                $reclamation->getStatut(),
                $reclamation->getId(),
                $reclamation->getReponse()
            );
        }

        return new JsonResponse(array('data' => $reclamationListJson));
    }


    public function loadReclamationUserDataTablesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $reclamationList = $em->getRepository('AdministrationBundle:Reclamation')->findAll();
        $reclamationListJson = array();
        foreach ($reclamationList as $reclamation) {
            $listTypesJson = array();
            $statusJson = array(
                "status" => $reclamation->getStatut(),
                "id" => $reclamation->getId()
            );
            $user = $this->getUser();
            if ($reclamation->getUser() == $user) {
                //$category = $em->getRepository('WSBundle:Category')->find($project->getCategory());
                array_push($reclamationListJson, array(
                    $reclamation->getCategorie(),
                    $reclamation-> getDateCreation()->format('m-d-Y')."",
//                    $reclamation->getEstValide(),
                    $reclamation->getTitre(),
                    $reclamation->getDescription(),
                    $statusJson,
                    $reclamation->getReponse()
                ));
            }
        }
        return new JsonResponse(array('data' => $reclamationListJson));
    }

    public function loadReclamationUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamationList = $em->getRepository('AdministrationBundle:Reclamation')->findAll();
        $reclamationListJson = array();

        $user = $em->getRepository('UserBundle:User')->find($id);

        foreach ($reclamationList as $reclamation) {
            $listTypesJson = array();
            $statusJson = array(
                "status" => $reclamation->getStatut(),
                "id" => $reclamation->getId()
            );
            if ($reclamation->getUser() == $user) {
                //$category = $em->getRepository('WSBundle:Category')->find($project->getCategory());
                array_push($reclamationListJson, array(
                    "categorie" => $reclamation->getCategorie(),
                    "dateCreation" => $reclamation->getDateCreation()->format("m-d-Y"),
//                    $reclamation->getEstValide(),
                    "titre" => $reclamation->getTitre(),
                    "description" => $reclamation->getDescription(),
                    "statut" =>$statusJson,
                    "reponse" => $reclamation->getReponse()
                ));
            }
        }
        return new JsonResponse(array('listeReclamations' => $reclamationListJson));
    }

    public function traiterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamation = $em->getRepository('AdministrationBundle:Reclamation')->find($request->get("id"));
        if ($reclamation->getStatut() == "Nouvelle") {
            $reclamation->setStatut("En cours de traitement");
            $em->persist($reclamation);
            $em->flush();
        }
        return $this->render('AdministrationBundle:Reclamation:admin_reclamation_traiter.html.twig', array("reclamation" => $reclamation));
    }

    public function AddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $user = $this->getUser();
            $categorie = $request->get('categorie');
            $titre = $request->get('titre');
            $description = $request->get('description');
            //$dateCreation = $request->get('dateCreation');
            $dateCreation = new \DateTime();
            $reclamation = new Reclamation();

            //$dateCreation = \DateTime::createFromFormat("d/m/Y H:i:s", $dateCreation);
            $reclamation->setCategorie($categorie);
//            $reclamation->setEstValide(false);
            $reclamation->setTitre($titre);
            $reclamation->setDescription($description);
            $reclamation->setDateCreation($dateCreation);
            $reclamation->setUser($user);
            $reclamation->setReponse("Pas de réponse");
            $reclamation->setStatut("Nouvelle");
            //$reclamation->setEstValide(true);

            $user = $reclamation->getUser();
            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('Nouvelle Reclamation de ' . $user->getNom());
            $notif->setMessage($titre);

            //$admins = $em->getRepository('UserBundle:User')->GetAdmins();
            //$manager->addNotification($admins, $notif, true);

            $em->persist($reclamation);
            $em->flush();
        }
        return $this->redirectToRoute('reclamation');
    }

    public function UpdateStatutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamation = $em->getRepository('AdministrationBundle:Reclamation')->find($request->get("id"));
        $reclamation->setStatut($request->get("statut"));
        $em->persist($reclamation);
        $em->flush();
        return new JsonResponse(array("type" => "success"));
    }

    public function UpdateReponseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamation = $em->getRepository('AdministrationBundle:Reclamation')->find($request->get("id"));
        $reclamation->setReponse($request->get("reponse"));
        $em->persist($reclamation);
        $em->flush();
        return new JsonResponse(array("type" => "success"));
    }

    public function FormAddAction()
    {
        return $this->render('TemplateBundle:Template:NouvelleReclamation.html.twig');
    }

    public function MarkAsSeenAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notifiableEntity = $em->getRepository('MgiletNotificationBundle:NotifiableEntity')->findBy(array('identifier' => $this->getUser()->getId()));
        $notifications = $em->getRepository('MgiletNotificationBundle:NotifiableNotification')->findBy(array('notifiableEntity' => $notifiableEntity[0]->getId()));
        foreach ($notifications as $notification) {
            $notification->setSeen(1);
            $em->remove($notification);
        }
        $em->flush();
        return $this->render('AdministrationBundle:Reclamation:admin_reclamation_index.html.twig');
    }



    public function AddReclamationAction($categorie,$titre,$description,$id)
    {
        $em = $this->getDoctrine()->getManager();

            $reclamation = new Reclamation();
            $user = $em->getRepository('UserBundle:User')->find($id);
            $reclamation->setCategorie($categorie);
            $reclamation->setTitre($titre);
            $reclamation->setDescription($description);
            $reclamation->setDateCreation(new \DateTime());
            $reclamation->setUser($user);
            $reclamation->setReponse("Pas de réponse");
            $reclamation->setStatut("Nouvelle");

        $em->persist($reclamation);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($reclamation);
        return new JsonResponse($formatted);
    }

    public function getAllUserAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userList = $em->getRepository('UserBundle:User')->findAll();
        $userListJson = array();

        foreach ($userList as $user) {

            array_push($userListJson, array(
                "username" => $user->getUsername(),
                "id"=> $user->getId()
            ));
        }
        return new JsonResponse(array('listUser' => $userListJson));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: mehdi
 * Date: 10/02/2018
 * Time: 19:46
 */

namespace AdministrationBundle\Controller;


use AdministrationBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdministrationBundle:User:admin_user_index.html.twig');
    }

    public function loadUserDataTablesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userList = $em->getRepository('UserBundle:User')->findAll();
        foreach ($userList as $user) {
            $userListJson[] = array(
                $user->getNom(),
                $user->getPrenom(),
                $user->getUsername(),
                $user->getId(),
                $user->isEnabled()
            );
        }
        return new JsonResponse(array( 'data' => $userListJson));
    }

    public function bannirAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($request->get("id"));
        $user->setEnabled(0);
        $em->persist($user);
        $em->flush();
        return $this->render('AdministrationBundle:User:admin_user_index.html.twig');
    }

    public function debannirAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($request->get("id"));
        $user->setEnabled(1);
        $em->persist($user);
        $em->flush();
        return $this->render('AdministrationBundle:User:admin_user_index.html.twig');
    }

}
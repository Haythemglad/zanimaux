<?php
/**
 * Created by PhpStorm.
 * User: mehdi
 * Date: 10/02/2018
 * Time: 19:09
 */

namespace AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Reclamation
 *
 * @ORM\Table(name="reclamation")
 * @ORM\Entity(repositoryClass="AdministrationBundle\Repository\ReclamationRepository")
 */

class Reclamation
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie",type="string")
     */
    private $categorie;
    /**
     *
     * @ORM\Column(name="date_creation",type="datetime",)
     */
    private $dateCreation;
    /**
     * @var string
     *
     * @ORM\Column(name="titre",type="string")
     */
    private $titre;
    /**
     * @var string
     *
     * @ORM\Column(name="description",type="string")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="statut",type="string")
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse",type="string")
     */
    private $reponse;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="AdministrationBundle\Entity\PieceJointe", mappedBy="user")
     */
    private $PieceJointe;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param String $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return String
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param String $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }



    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return mixed
     */
    public function getPieceJointe()
    {
        return $this->PieceJointe;
    }

    /**
     * @param mixed $PieceJointe
     */
    public function setPieceJointe($PieceJointe)
    {
        $this->PieceJointe = $PieceJointe;
    }



    /**
     * @return String
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param String $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return String
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * @param String $reponse
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}
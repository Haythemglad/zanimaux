<?php

namespace AnnonceBundle\Controller;

use AnnonceBundle\Entity\Signalement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SignalementController extends Controller
{

    public function SignalerAction($id, Request $request )
    {
        $Signalement = new Signalement();
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == "POST") {

            $user = $this->getUser();

            $e = $request->get('signalement');


            $Signalement->setUser($user);
            $Signalement->setType($e);
            $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
            $Signalement->setAnnonce($annonce);

            $em->persist($Signalement);
            $em->flush();
            return $this->redirectToRoute('_Signaler',array('id'=>$id));

        }
        return $this->render('AnnonceBundle:Annonce:Signaler.html.twig'
        );
    }

}

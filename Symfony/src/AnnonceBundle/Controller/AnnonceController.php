<?php

namespace AnnonceBundle\Controller;

use AnnonceBundle\AnnonceBundle;
use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Form\AnnonceType;
use AnnonceBundle\Form\Annonce2Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Repository\UserRepository;
use AnnonceBundle\Repository\SignalementRepository;
use UserBundle\UserBundle;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class AnnonceController extends Controller
{
    public function AffichageAction()
    {

        $em = $this->getDoctrine()->getManager();
        $annonces = $em->getRepository('AnnonceBundle:Annonce')->findBy(array('user' => $this->getUser()));
        return $this->render('AnnonceBundle:Annonce:affichageUser.html.twig', array(
            'annonces' => $annonces
        ));
    }
    public function AffichageJeVeuxAdopterAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $demande = $em->getRepository('AnnonceBundle:Demande')->findOneBy(array("user" => $this->getUser(), "annonce" => $annonce));
        if ($demande == null){
            $annonce -> setDemande(0);
        }
        else {
            $annonce -> setDemande(1);
        }
        return $this->render('AnnonceBundle:Annonce:JeVeuxAdopter.html.twig', array(
            'annonce' => $annonce


        ));

    }

    public function AffichageJeVeuxAcheterAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $demande = $em->getRepository('AnnonceBundle:Demande')->findOneBy(array("user" => $this->getUser(), "annonce" => $annonce));
        if ($demande == null){
            $annonce -> setDemande(0);
        }
        else {
            $annonce -> setDemande(1);
        }
        return $this->render('AnnonceBundle:Annonce:JeVeuxAcheter.html.twig', array(
            'annonce' => $annonce


        ));


    }


    public function AffichageItsMineAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $demande = $em->getRepository('AnnonceBundle:Demande')->findOneBy(array("user" => $this->getUser(), "annonce" => $annonce));
        if ($demande == null){
            $annonce -> setDemande(0);
        }
        else {
            $annonce -> setDemande(1);
        }
        return $this->render('AnnonceBundle:Annonce:itsMine.html.twig', array(
            'annonce' => $annonce


        ));


    }





    public function AffichageMenuAction()
    {

        return $this->render('AnnonceBundle:Annonce:MenuAnnonce.html.twig');


    }

    public function AffichageAnnonceAdminAction()
    {

        $em = $this->getDoctrine()->getManager();
        $annonces = $em->getRepository('AnnonceBundle:Annonce')->findAll();
        foreach ($annonces as $annonce) {
            $scoreT1 = $em->getRepository('AnnonceBundle:Signalement')->getSignalementCountByType($annonce->getId(), 1);
            $scoreT2 = $em->getRepository('AnnonceBundle:Signalement')->getSignalementCountByType($annonce->getId(), 2);
            $scoreT3 = $em->getRepository('AnnonceBundle:Signalement')->getSignalementCountByType($annonce->getId(), 3);
            $scoreT4 = $em->getRepository('AnnonceBundle:Signalement')->getSignalementCountByType($annonce->getId(), 4);
            $scoreT5 = $em->getRepository('AnnonceBundle:Signalement')->getSignalementCountByType($annonce->getId(), 5);


            $scoreFinale = (intval($scoreT1) * 5) + (intval($scoreT2) * 4) + (intval($scoreT3) * 3) + (intval($scoreT4) * 2) + (intval($scoreT5) * 1);
            $annonce->setSignalementCount($scoreFinale);


        }

        return $this->render('AnnonceBundle:Annonce:AdminAffichage.html.twig', array(
            'annonces' => $annonces
        ));


    }

    public function AffichageAnnonceVenteAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $Listeannonces = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ( 'categorie' => "Vente",'annonceFin'=>0));


        /**
         * @var $paginator \knp\component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $Listeannonces,
            $request->query
                ->getInt('page', 1),
            $request->query->getInt('limit', 3)
        );

        return $this->render('AnnonceBundle:Annonce:affichageVente.html.twig', array(
            'annoncesV' => $result,
        ));
    }

    public function AffichageAnnonceAdoptionAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $Listeannonces = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ('estvalide' => 1, 'categorie' => "Adoption",'annonceFin'=>0));


        /**
         * @var $paginator \knp\component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $Listeannonces,
            $request->query
                ->getInt('page', 1),
            $request->query->getInt('limit', 3)
        );

        return $this->render('AnnonceBundle:Annonce:affichageAdoption.html.twig', array(
            'annoncesA' => $result,
        ));
    }

    public function AffichageAnnonceLostAndFoundAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $Listeannonces = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ('estvalide' => 1, 'categorie' => "LostAndFound",'annonceFin'=>0));


        /**
         * @var $paginator \knp\component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $Listeannonces,
            $request->query
                ->getInt('page', 1),
            $request->query->getInt('limit', 3)
        );

        return $this->render('AnnonceBundle:Annonce:affichageLostAndFound.html.twig', array(
            'annoncesL' => $result,
        ));
    }


    public function AjoutVenteeAction(Request $request)

    {
        $em = $this->getDoctrine()->getManager();
        $annonce = new Annonce();

        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);
        if ($form->isValid()) {

            $annonce->setUser($this->getUser());


            $annonce->setCategorie('Vente');
            $annonce->setDatecreation(new \DateTime('now'));
            //$annonce->UploadProfilePicture();

            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('_affichage');

        }

        return $this->render('AnnonceBundle:Annonce:ajoutVentee.html.twig', array('f' => $form->createView()));


    }

    public function AjoutAutreeAction(Request $request)

    {
        $em = $this->getDoctrine()->getManager();
        $annonce = new Annonce();

        $form = $this->createForm(Annonce2Type::class, $annonce);
        $form->handleRequest($request);
        if ($form->isValid()) {

            $annonce->setUser($this->getUser());


            $annonce->setDatecreation(new \DateTime('now'));
            $annonce->UploadProfilePicture();

            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('_affichage');

        }

        return $this->render('AnnonceBundle:Annonce:ajoutAutre.html.twig', array('f' => $form->createView()));


    }


    public function SupprimerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $em->remove($annonce);
        $em->flush();
        return $this->redirectToRoute('_affichage');
    }

    public function SupprimerAdminAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $em->remove($annonce);
        $em->flush();
        return $this->redirectToRoute('_affichageAdmin');
    }

    public function ModifierVenteAction($id, Request $request)
    {
        $annonce = new Annonce();
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        if ($request->getMethod() == "POST") {

            $a = $request->get('categorie');


            $c = $request->get('animal');
            $d = $request->get('description');
            $e = $request->get('race');
            $o = $request->get('prix');


            $annonce->setCategorie($a);
            $annonce->setDatecreation(new \DateTime('now'));
            $annonce->setAnimal($c);
            $annonce->setUser($this->getUser());
            $annonce->setDescription($d);
            $annonce->setRace($e);
            $annonce->setPrix($o);
            $annonce->setEstvalide(0);


            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('_affichage');


        }

        return $this->render('AnnonceBundle:Annonce:modifierVente.html.twig', array('annonces' => $annonce)
        );
    }

    public function ModifierAutreAction($id, Request $request)
    {
        $annonce = new Annonce();
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        if ($request->getMethod() == "POST") {

            $a = $request->get('categorie');


            $c = $request->get('animal');
            $d = $request->get('description');
            $e = $request->get('race');


            $annonce->setCategorie($a);
            $annonce->setDatecreation(new \DateTime('now'));
            $annonce->setAnimal($c);
            $annonce->setUser($this->getUser());
            $annonce->setDescription($d);
            $annonce->setRace($e);
            $annonce->setEstvalide(0);


            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('_affichage');


        }

        return $this->render('AnnonceBundle:Annonce:modifierAutre.html.twig', array('annonces' => $annonce)
        );
    }

    public function RechercheVenteAction($price1,$price2)


    {$annonce = new Annonce();
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->searchAnnonceByPrice($price1,$price2);
        return $this->render('AnnonceBundle:Annonce:affichageVente.html.twig', array("annonces"=>$annonce));


    }


    public function ValiderAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);

        $annonce->setEstvalide(1);
        $em->flush();
        return $this->redirectToRoute('_affichageAdmin');
    }
    public function AffichageSignalerAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $signalement = $em->getRepository('AnnonceBundle:Signalement')->findOneBy(array("user" => $this->getUser(), "annonce" => $annonce));
        if ($signalement == null){
            $annonce -> setHasBeenReported(0);
        }
        else {
            $annonce -> setHasBeenReported(1);
        }
        return $this->render('AnnonceBundle:Annonce:Signaler.html.twig', array(
            'annonce' => $annonce
        ));


    }



}

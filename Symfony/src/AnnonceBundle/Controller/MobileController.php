<?php

namespace AnnonceBundle\Controller;

use AnnonceBundle\Entity\Annonce;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MobileController extends Controller



{
    public function AffichageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $listeAnnonce = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ('categorie' => "Vente"));
        $listAnnonceJson = array();
        foreach ($listeAnnonce as $annonce) {

            $listAnnonceJson[] = array(

                "Id" => $annonce->getId(),
                "User" => $annonce->getUser()->getId(),

                "Categorie" => $annonce->getCategorie(),
                "Animal" => $annonce->getAnimal(),
                "Race" => $annonce->getRace(),
                "Description" => $annonce->getDescription(),

                "Prix" => $annonce->getPrix(),
                "Image" => $annonce->getImage(),

            );
        }


        return new JsonResponse(array("listAnnonce" => $listAnnonceJson));
    }

    public function AffichageMesAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $listeAnnonce = $em->getRepository('AnnonceBundle:Annonce')->findBy(array('user' => $id ));
        $listAnnonceJson1 = array();
        foreach ($listeAnnonce as $annonce) {

            $listAnnonceJson1[] = array(
                "Id"=>$annonce->getId(),

                "User" => $annonce->getUser()->getId(),

                "Categorie" => $annonce->getCategorie(),
                "Animal" => $annonce->getAnimal(),
                "Race" => $annonce->getRace(),
                "Description" => $annonce->getDescription(),

                "Prix" => $annonce->getPrix(),
                "Image" => $annonce->getImage(),
            );
        }



        return new JsonResponse(array("listAnnonce" => $listAnnonceJson1));
    }

    public function Affichage1Action()
    {
        $em = $this->getDoctrine()->getManager();
        $listeAnnonce1 = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ('categorie' => "Adoption"));
        $listAnnonceJson1 = array();
        foreach ($listeAnnonce1 as $annonce) {

            $listAnnonceJson1[] = array(
                "Id"=>$annonce->getId(),

                "User" => $annonce->getUser()->getId(),

                "Categorie" => $annonce->getCategorie(),
                "Animal" => $annonce->getAnimal(),
                "Race" => $annonce->getRace(),
                "Description" => $annonce->getDescription(),

                "Prix" => $annonce->getPrix(),
                "Image" => $annonce->getImage(),

            );
        }


        return new JsonResponse(array("listAnnonce" => $listAnnonceJson1));
    }

    public function Affichage2Action()
    {
        $em = $this->getDoctrine()->getManager();
        $listeAnnonce2 = $em->getRepository('AnnonceBundle:Annonce')->findBy(array
        ('categorie' => "LostAndFound"));
        $listAnnonceJson2 = array();
        foreach ($listeAnnonce2 as $annonce) {

            $listAnnonceJson2[] = array(
                "id"=>$annonce->getId(),

                "Categorie" => $annonce->getCategorie(),
                "Animal" => $annonce->getAnimal(),
                "Race" => $annonce->getRace(),
                "Description" => $annonce->getDescription(),


                "Image" => $annonce->getImage(),

            );
        }


        return new JsonResponse(array("listAnnonce" => $listAnnonceJson2));
    }


    public function AjouterAction(Request $request,$id)
    {


        $em = $this->getDoctrine()->getManager();
        $annonce = new Annonce ();

        $user = $em->getRepository('UserBundle:User')->find($id);

        $annonce->setUser($user);
        $annonce->setAnimal($request->get('animal'));
        $annonce->setCategorie($request->get('categorie'));
        $annonce->setRace($request->get('race'));

        $annonce->setDescription($request->get('description'));
        $annonce->setPrix($request->get('prix'));
        $annonce->setImage($request->get('image'));


        $em->persist($annonce);
        $em->flush();

        $serializer = new Serializer(new ObjectNormalizer());
        $formatted = $serializer->normalize($annonce);
        return new JsonResponse($formatted);


    }

    public function SupprimerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
        $em->remove($annonce);
        $em->flush();
        $serializer = new Serializer(new ObjectNormalizer());
        $formatted = $serializer->normalize($annonce);
        return new JsonResponse($formatted);
    }

    public function ModifierAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);


            $a = $request->get('categorie');


            $c = $request->get('animal');
            $d = $request->get('description');
            $e = $request->get('race');
            $o = $request->get('prix');


            $annonce->setCategorie($a);
           // $annonce->setDatecreation(new \DateTime('now'));
            $annonce->setAnimal($c);
           // $annonce->setUser($this->getUser());
            $annonce->setDescription($d);
            $annonce->setRace($e);
            $annonce->setPrix($o);
            //$annonce->setEstvalide(0);


            $em->persist($annonce);
            $em->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($annonce);
            return new JsonResponse($formatted);



    }
}

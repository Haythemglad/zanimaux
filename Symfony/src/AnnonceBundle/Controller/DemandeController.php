<?php

namespace AnnonceBundle\Controller;

use AnnonceBundle\AnnonceBundle;
use AnnonceBundle\Entity\Demande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DemandeController extends Controller
{


    public function DemandeAdoptionAction($id,Request $request)
    {
        $demande= new Demande();
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod()=="POST")
        {
            $x=$request->get('text1');
            $y=$request->get('text2');
            $z=$request->get('text3');
            $n=$request->get('num');
            $user = $this->getUser();

            $demande->setQ1($x);
            $demande->setQ2($y);
            $demande->setQ3($z);
            $demande->setNumTel($n);

            $demande->setUser($user);

            $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
            $demande->setAnnonce($annonce);
            $demande->setCategorie('Adoption');

            $em=$this->getDoctrine()->getManager();
            $em->persist($demande);
            $em->flush();

            return $this->redirectToRoute('_affichageAdoption');

        }

        return $this->render('AnnonceBundle:Annonce:JeVeuxAdopter.html.twig', array(
            // ...
        ));
    }
    public function DemandeAchatAction($id,Request $request)
    {
        { $demande= new Demande();
            $em = $this->getDoctrine()->getManager();
            if($request->getMethod()=="POST")
            {
                var_dump('mehdi');
            $x=$request->get('text7');
            $y=$request->get('text8');
            $z=$request->get('text9');
            $n=$request->get('num3');
            $user = $this->getUser();

            $demande->setQ7($x);
            $demande->setQ8($y);
            $demande->setPrixSecondaire($z);
            $demande->setNumTel($n);

            $demande->setUser($user);

            $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
            $demande->setAnnonce($annonce);
            $demande->setCategorie('Vente');

            $em=$this->getDoctrine()->getManager();
            $em->persist($demande);
            $em->flush();

            return $this->redirectToRoute('_affichageVente');

        }

        }

        return $this->render('AnnonceBundle:Annonce:JeVeuxAcheter.html.twig', array(
            // ...
        ));
    }
    public function DemandeRecuperationAction($id,Request $request)
    {
        $demande= new Demande();
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod()=="POST")
        {
            $x=$request->get('text4');
            $y=$request->get('text5');
            $z=$request->get('text6');

            $n=$request->get('num2');
            $user = $this->getUser();

            $demande->setQ4($x);
            $demande->setQ5($y);
            $demande->setQ6($z);
            $demande->setNumTel($n);


            $demande->setUser($user);

            $annonce = $em->getRepository('AnnonceBundle:Annonce')->find($id);
            $demande->setAnnonce($annonce);
            $demande->setCategorie('LostAndFound');


            $em=$this->getDoctrine()->getManager();
            $em->persist($demande);
            $em->flush();

            return $this->redirectToRoute('_affichageLostAndFound');

        }

        return $this->render('AnnonceBundle:Annonce:itsMine.html.twig', array(
            // ...
        ));
    }


    public function AffichageInteractionsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $demandes = $em->getRepository('AnnonceBundle:Demande')->findBy(array('user' => $this->getUser()));


        return $this->render('AnnonceBundle:Annonce:affichageInteractions.html.twig', array(
            'losts' => $demandes
        ));
    }


    public function ValiderAction($id,$annonce)
    {
        $em = $this->getDoctrine()->getManager();
        $annonce = $em->getRepository('AnnonceBundle:Annonce')->find(array('id'=>$annonce));


        $demande = $em->getRepository('AnnonceBundle:Demande')->find($id);

        $annonce->setAnnonceFin(1);

        $demande->setvalide(1);
        $em->flush();
        return $this->redirectToRoute('_affichage3');
    }
    public function SupprimerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('AnnonceBundle:Demande')->find($id);
        $em->remove($demande);
        $em->flush();
        return $this->redirectToRoute('_affichage3');
    }


}

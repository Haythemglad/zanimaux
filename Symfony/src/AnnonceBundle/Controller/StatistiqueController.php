<?php

namespace AnnonceBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AnnonceBundle\AnnonceBundle;
use AnnonceBundle\Entity\Annonce;
use AnnonceBundle\Form\AnnonceType;
use AnnonceBundle\Form\Annonce2Type;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Repository\UserRepository;
use AnnonceBundle\Repository\SignalementRepository;
use UserBundle\UserBundle;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class StatistiqueController extends Controller
{
    public function StatAction()
    {

    }
    public function indexAction()
    {
        $m = $this->getDoctrine()->getManager();
        $mark = $m->getRepository('AnnonceBundle:Signalement')->findByType(6);
        $type=array();
        $s=0;
        foreach ($mark as $type1)
        {
            array_push($type,$type1);
            $s=$s+1;
        }
        $mark1 = $m->getRepository('AnnonceBundle:Signalement')->findByType(4);
        $t=array();
        $y=0;
        foreach ($mark1 as $type2)
        {
            array_push($t,$type2);
            $y=$y+1;
        }
        $mark2 = $m->getRepository('AnnonceBundle:Signalement')->findByType(3);
        $ps=array();
        $p=0;
        foreach ($mark2 as $type3)
        {
            array_push($ps,$type3);
            $p=$p+1;
        }
        $mark3 = $m->getRepository('AnnonceBundle:Signalement')->findByType(2);
        $ps=array();
        $pc=0;
        foreach ($mark3 as $type4)
        {
            array_push($ps,$type4);
            $pc=$pc+1;
        }
        $mark4 = $m->getRepository('AnnonceBundle:Signalement')->findByType(1);
        $sa=array();
        $c=0;
        foreach ($mark4 as $type5)
        {
            array_push($sa,$type5);
            $c=$c+1;
        }

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['Contenu inaproprie',     $s],
                ['informations incorrects',      $y],
                ['ce nest pas interessant',  $p],
                ['spam', $pc],
                ['Autres',    $c],

            ]
        );
        $pieChart->getOptions()->setTitle('Satistique des Type de Signalement');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        return $this->render('AnnonceBundle:Annonce:statistique.html.twig', array('piechart' => $pieChart));
    }
    public function index2Action()
    {
        $m = $this->getDoctrine()->getManager();
        $mark = $m->getRepository('AnnonceBundle:Annonce')->findByCategorie("Vente");
        $type=array();
        $s=0;
        foreach ($mark as $type1)
        {
            array_push($type,$type1);
            $s=$s+1;
        }
        $mark1 = $m->getRepository('AnnonceBundle:Annonce')->findByCategorie('Adoption');
        $t=array();
        $y=0;
        foreach ($mark1 as $type2)
        {
            array_push($t,$type2);
            $y=$y+1;
        }
        $mark2 = $m->getRepository('AnnonceBundle:Annonce')->findByCategorie('LostAndFound');
        $ps=array();
        $p=0;
        foreach ($mark2 as $type3)
        {
            array_push($ps,$type3);
            $p=$p+1;
        }


        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['Vente',     $s],
                ['Adoption',      $y],
                ['LostAndFound',  $p],


            ]
        );
        $pieChart->getOptions()->setTitle('Statistique par categorie des annonces');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);


        return $this->render('AnnonceBundle:Annonce:statistique2.html.twig', array('piechart' => $pieChart));
    }
    public function stat3Action()
    {
        $m = $this->getDoctrine()->getManager();
        $mark = $m->getRepository('AnnonceBundle:Demande')->findByType(6);
        $type=array();
        $s=0;
        foreach ($mark as $type1)
        {
            array_push($type,$type1);
            $s=$s+1;
        }
        $mark1 = $m->getRepository('AnnonceBundle:Signalement')->findByType(4);
        $t=array();
        $y=0;
        foreach ($mark1 as $type2)
        {
            array_push($t,$type2);
            $y=$y+1;
        }
        $mark2 = $m->getRepository('AnnonceBundle:Signalement')->findByType(3);
        $ps=array();
        $p=0;
        foreach ($mark2 as $type3)
        {
            array_push($ps,$type3);
            $p=$p+1;
        }
        $mark3 = $m->getRepository('AnnonceBundle:Signalement')->findByType(2);
        $ps=array();
        $pc=0;
        foreach ($mark3 as $type4)
        {
            array_push($ps,$type4);
            $pc=$pc+1;
        }
        $mark4 = $m->getRepository('AnnonceBundle:Signalement')->findByType(1);
        $sa=array();
        $c=0;
        foreach ($mark4 as $type5)
        {
            array_push($sa,$type5);
            $c=$c+1;
        }

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['Contenu inaproprie',     $s],
                ['informations incorrects',      $y],
                ['ce nest pas interessant',  $p],
                ['spam', $pc],
                ['Autres',    $c],

            ]
        );
        $pieChart->getOptions()->setTitle('Satistique des Type de Signalement');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        return $this->render('AnnonceBundle:Annonce:statistique3.html.twig', array('piechart' => $pieChart));
    }

}

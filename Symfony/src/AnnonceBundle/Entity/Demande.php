<?php

namespace AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\DemandeRepository")
 */
class Demande
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AnnonceBundle\Entity\Annonce")
     * @ORM\JoinColumn(name="annonce_id",referencedColumnName="id",onDelete="CASCADE")
     */

    private $annonce;

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getAnnonce()
    {
        return $this->annonce;
    }

    /**
     * @param int $annonce
     */
    public function setAnnonce($annonce)
    {
        $this->annonce = $annonce;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }




    /**
     * @var string
     *
     * @ORM\Column(name="q1", type="string", length=255,nullable=true)
     */
    private $q1;

    /**
     * @var string
     *
     * @ORM\Column(name="q2", type="string", length=255,nullable=true)
     */
    private $q2;

    /**
     * @var string
     *
     * @ORM\Column(name="q3", type="string", length=255,nullable=true)
     */
    private $q3;

    /**
     * @var string
     *
     * @ORM\Column(name="q4", type="string", length=255,nullable=true)
     */
    private $q4;

    /**
     * @var string
     *
     * @ORM\Column(name="q5", type="string", length=255,nullable=true)
     */
    private $q5;

    /**
     * @var string
     *
     * @ORM\Column(name="q6", type="string", length=255,nullable=true)
     */
    private $q6;

    /**
     * @var float
     *
     * @ORM\Column(name="prixSecondaire", type="float", length=255,nullable=true)
     */
    private $prixSecondaire;

    /**
     * @var string
     *
     * @ORM\Column(name="q7", type="string", length=255,nullable=true)
     */
    private $q7;
    /**
     * @var string
     *
     * @ORM\Column(name="q8", type="string", length=255,nullable=true)
     */
    private $q8;

    /**
     * @var string
     *
     * @ORM\Column(name="numTel", type="string", length=255)
     */
    private $numTel;

    /**
     * @var bool
     *
     * @ORM\Column(name="valide", type="boolean")
     */

    private $valide =false;

    /**
     * @return boolean
     */
    public function isValide()
    {
        return $this->valide;
    }

    /**
     * @param boolean $valide
     */
    public function setValide($valide)
    {
        $this->valide = $valide;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set q1
     *
     * @param string $q1
     *
     * @return Demande
     */
    public function setQ1($q1)
    {
        $this->q1 = $q1;

        return $this;
    }

    /**
     * Get q1
     *
     * @return string
     */
    public function getQ1()
    {
        return $this->q1;
    }

    /**
     * Set q2
     *
     * @param string $q2
     *
     * @return Demande
     */
    public function setQ2($q2)
    {
        $this->q2 = $q2;

        return $this;
    }

    /**
     * Get q2
     *
     * @return string
     */
    public function getQ2()
    {
        return $this->q2;
    }

    /**
     * Set q3
     *
     * @param string $q3
     *
     * @return Demande
     */
    public function setQ3($q3)
    {
        $this->q3 = $q3;

        return $this;
    }

    /**
     * Get q3
     *
     * @return string
     */
    public function getQ3()
    {
        return $this->q3;
    }

    /**
     * Set q4
     *
     * @param string $q4
     *
     * @return Demande
     */
    public function setQ4($q4)
    {
        $this->q4 = $q4;

        return $this;
    }

    /**
     * Get q4
     *
     * @return string
     */
    public function getQ4()
    {
        return $this->q4;
    }

    /**
     * Set q5
     *
     * @param string $q5
     *
     * @return Demande
     */
    public function setQ5($q5)
    {
        $this->q5 = $q5;

        return $this;
    }

    /**
     * Get q5
     *
     * @return string
     */
    public function getQ5()
    {
        return $this->q5;
    }

    /**
     * Set q6
     *
     * @param string $q6
     *
     * @return Demande
     */
    public function setQ6($q6)
    {
        $this->q6 = $q6;

        return $this;
    }

    /**
     * Get q6
     *
     * @return string
     */
    public function getQ6()
    {
        return $this->q6;
    }

    /**
     * Set prixSecondaire
     *
     * @param string $prixSecondaire
     *
     * @return Demande
     */
    public function setPrixSecondaire($prixSecondaire)
    {
        $this->prixSecondaire = $prixSecondaire;

        return $this;
    }

    /**
     * Get prixSecondaire
     *
     * @return string
     */
    public function getPrixSecondaire()
    {
        return $this->prixSecondaire;
    }

    /**
     * Set numTel
     *
     * @param string $numTel
     *
     * @return Demande
     */
    public function setNumTel($numTel)
    {
        $this->numTel = $numTel;

        return $this;
    }

    /**
     * Get numTel
     *
     * @return string
     */
    public function getNumTel()
    {
        return $this->numTel;
    }





    /**
     * @return string
     */
    public function getQ7()
    {
        return $this->q7;
    }

    /**
     * @param string $q7
     */
    public function setQ7($q7)
    {
        $this->q7 = $q7;
    }

    /**
     * @return string
     */
    public function getQ8()
    {
        return $this->q8;
    }

    /**
     * @param string $q8
     */
    public function setQ8($q8)
    {
        $this->q8 = $q8;
    }





    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }
}

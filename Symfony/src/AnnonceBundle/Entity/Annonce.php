<?php

namespace AnnonceBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;


use Doctrine\ORM\Mapping as ORM;



/**
 * Annonce
 *
 * @ORM\Table(name="annonce")
 * @ORM\Entity(repositoryClass="AnnonceBundle\Repository\AnnonceRepository")
 */
class Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecreation", type="datetime", length=255)
     */
    private $datecreation;
    /**
     * @var bool
     *
     * @ORM\Column(name="estvalide", type="boolean")
     */

    private $estvalide =false;

    /**
     * @var bool
     *
     * @ORM\Column(name="annonceFin", type="boolean")
     */

    private $annonceFin =false;

    private $signalementCount ;


    private $hasBeenReported;

    private $demande;
    private $demande1;
    private $demande2;

    /**
     * @return mixed
     */
    public function getDemande1()
    {
        return $this->demande1;
    }

    /**
     * @param mixed $demande1
     */
    public function setDemande1($demande1)
    {
        $this->demande1 = $demande1;
    }

    /**
     * @return mixed
     */
    public function getDemande2()
    {
        return $this->demande2;
    }

    /**
     * @param mixed $demande2
     */
    public function setDemande2($demande2)
    {
        $this->demande2 = $demande2;
    }


    /**
     * @return mixed
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * @param mixed $demande
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="animal", type="string", length=255)
     */
    private $animal;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */


    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=255,nullable=true)
     */

    private $race;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float",nullable=true)
     */
    private $prix;

    /**
     * @return string
     */
    public function getProprietaire()
    {
        return $this->proprietaire;
    }

    /**
     * @param string $proprietaire
     */
    public function setProprietaire($proprietaire)
    {
        $this->proprietaire = $proprietaire;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="proprietaire", type="string", length=255)
     */



    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }



    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Ajouter une image jpg")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $image;




    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getAnimal()
    {
        return $this->animal;
    }

    /**
     * @param string $animal
     */
    public function setAnimal($animal)
    {
        $this->animal = $animal;
    }

    /**
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param string $race
     */
    public function setRace($race)
    {
        $this->race = $race;
    }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Annonce
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set datecreation
     *
     * @param string $datecreation
     *
     * @return Annonce
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * Get datecreation
     *
     * @return string
     */
    public function getDatecreation()
    {
        return $this->datecreation;

    }

    /**
     * @return boolean
     */
    public function isEstvalide()
    {
        return $this->estvalide;
    }

    /**
     * @param boolean $estvalide
     */
    public function setEstvalide($estvalide)
    {
        $this->estvalide = $estvalide;
    }




    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return boolean
     */
    public function isAnnonceFin()
    {
        return $this->annonceFin;
    }

    /**
     * @param boolean $annonceFin
     */
    public function setAnnonceFin($annonceFin)
    {
        $this->annonceFin = $annonceFin;
    }

    /**
     * @return mixed
     */
    public function getSignalementCount()
    {
        return $this->signalementCount;
    }

    /**
     * @param mixed $signalementCount
     */
    public function setSignalementCount($signalementCount)
    {
        $this->signalementCount = $signalementCount;
    }

    /**
     * @return mixed
     */
    public function getHasBeenReported()
    {
        return $this->hasBeenReported;
    }

    /**
     * @param mixed $hasBeenReported
     */
    public function setHasBeenReported($hasBeenReported)
    {
        $this->hasBeenReported = $hasBeenReported;
    }




}


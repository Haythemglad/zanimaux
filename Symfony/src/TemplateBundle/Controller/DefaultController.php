<?php

namespace TemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TemplateBundle:Default:index.html.twig');
    }

    public function EventsAction()
    {
        return $this->render('DaliiBundle:Events:index.html.twig');
    }

    public function LayoutAction()
    {
        return $this->render('TemplateBundle::Layout.html.twig');
    }
    public function Index1Action()
    {
        return $this->render('TemplateBundle:Template:index.html.twig');
    }
    public function ServiceAction()
    {
        return $this->render('TemplateBundle:Template:Service.html.twig');
    }
    public function ForumAction()
    {
        return $this->render('TemplateBundle:Template:Forum.html.twig');
    }
    public function RendezvousAction()
    {
        return $this->render('TemplateBundle:Template:Rendezvous.html.twig');
    }
    public function AnnoncesAction()
    {
        return $this->render('AnnonceBundle:Annonce:MenuAnnonce.html.twig');
    }

    public function InfoUserAction()
    {
        return $this->render('TemplateBundle:Default:info_user.html.twig');
    }

    public function MenuAction()
    {
        $em = $this->getDoctrine()->getManager();

        $menus = $em->getRepository('TemplateBundle:menu')->findAll();
        return $this->render('TemplateBundle:Default:menu.html.twig',array('menus'=>$menus));
    }

    public function RDVAction()
    {
        return $this->render('TemplateBundle:Template:Rdv.html.twig');
    }

    public function ReclamationAction()
    {
        return $this->render('TemplateBundle:Template:Reclamation.html.twig');
    }




}

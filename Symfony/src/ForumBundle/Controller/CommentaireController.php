<?php

namespace ForumBundle\Controller;

use ForumBundle\Entity\Commentaire;
use ForumBundle\Form\CommentaireType;
use ForumBundle\Form\ForumType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CommentaireController extends Controller
{
    public function AddcommAction(Request $request)
    {

        $comment = new Commentaire();
        $id = $request->get('id');
        $comment->setCree(new \DateTime('now'));
        $forms = $this->createForm(CommentaireType::class,$comment);
        $forms->handleRequest($request);
        $user=$this->getUser();

        if ($forms->isSubmitted() && $forms->isValid()) {

            $comment->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('Comment_add ');
        }
        return $this->render("ForumBundle:topic.html.twig",
            array(''=>$forms->createView()));
    }




    public function updateAction(Request $request)
    {


        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository("ForumBundle:Commentaire")->find($id);
        $forum->setModifiee(new \DateTime('now'));
        $form = $this->createForm(CommentaireType::class, $forum);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($forum);
            $em->flush();

            return $this->redirectToRoute('forum_show');
        }
        return $this->render("@Forum/Sujet/edit_comment.html.twig", array("forms" => $form->createView()));

    }
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $Article = $em->getRepository("ForumBundle:Commentaire")->find($id);
        $em->remove($Article);
        $em->flush();
        return $this->redirectToRoute('forum_show');
    }
}

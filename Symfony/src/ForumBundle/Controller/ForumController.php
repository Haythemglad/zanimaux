<?php

namespace ForumBundle\Controller;

use blackknight467\StarRatingBundle\Form\RatingType;
use ForumBundle\Form\RateType;
use ForumBundle\Entity\Commentaire;
use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Rating;
use ForumBundle\Form\CommentaireType;
use ForumBundle\Form\ForumType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ForumController extends Controller
{

    public function AddAction(Request $request)
    {

        $forum = new Forum();
        $forum->setCree(new \DateTime('now'));
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);
        $user = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {

            $forum->setAuteur($user);

            $file = $form['image']->getData();

            $file->move("images/", $file->getClientOriginalName());
            $forum->setImage("images/" . $file->getClientOriginalName());

            $em = $this->getDoctrine()->getManager();
            $em->persist($forum);
            $em->flush();
            return $this->redirectToRoute('forum_homepage');
        }
        $a=$this->getUser();
        return $this->render("ForumBundle:Sujet:new_topic.html.twig", array('user'=>$a,'form' => $form->createView()));
    }

    public function ShowAction(Request $request)
    {
        $m = $this->getDoctrine()->getManager();
        $dql   = "SELECT a FROM ForumBundle:Forum a WHERE a.Fenabled=FALSE ";
        $forum=$m->createQuery($dql);
        /**
         * @var  $paginator \Knp\Component\Pager\Paginator
         **/
         $paginator  = $this->get('knp_paginator');
         $result= $paginator->paginate(
            $forum,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 2)
          );
            $a=$this->getUser();
        $Rating = $m->getRepository('ForumBundle:Rating')->AVGRating();
        return $this->render("@Forum/Sujet/Show_topic.html.twig", array('user' => $a,'forum' => $result,'rating' => $Rating));

    }

    public function LoadDataAction(Request $request)
    {
        $m = $this->getDoctrine()->getManager();
        $dql   = "SELECT a FROM ForumBundle:Forum a WHERE a.Fenabled=FALSE ";
        $forums=$m->createQuery($dql)->getResult();

        $forumsJson = array();
        foreach ($forums as $forum){
            $dateCree = '';
            if($forum->getCree() != null){
                $dateCree =  $forum->getCree()->format("Y/m/d H:m:s");
            }
            $dateModifee = '';
            if($forum->getModifiee() != null){
                $dateModifee =  $forum->getModifiee()->format("Y/m/d H:m:s");
            }
            $forumsJson[] = array(
                "titre" => $forum->getTitre(),
                "blog"=>$forum->getBlog(),
                "auteur"=>$forum->getAuteur(),
                "tags"=>$forum->getTags(),
                "Modifee"=>$dateModifee,
                "Cree"=>$dateCree
            );
        }
        return new JsonResponse(array('listesForum' => $forumsJson));

    }

    public function updateAction(Request $request ,$id)
    {


        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository("ForumBundle:Forum")->find($id);
        $forum->setModifiee(new \DateTime('now'));
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $file = $form['image']->getData();

            $file->move("images/", $file->getClientOriginalName());
            $forum->setImage("images/" . $file->getClientOriginalName());

            $em->persist($forum);
            $em->flush();

            return $this->redirectToRoute('forum_show');
        }
        return $this->render("@Forum/Sujet/Update_topic.html.twig", array("form" => $form->createView()));

    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $Article = $em->getRepository("ForumBundle:Forum")->find($id);
        $em->remove($Article);
        $em->flush();
        return $this->redirectToRoute('forum_show');
    }

    public function ShowTopicAction(Request $request,$id)
    {
        //  $id = $request->get('id');
        $m = $this->getDoctrine()->getManager();
        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository("ForumBundle:Forum")->find($id);

        $titre = $forum->getTitre();
        $cree = $forum->getCree();
        $auteur = $forum->getAuteur();
        $blog = $forum->getBlog();
        $idf = $forum->getId();
        $image = $forum->getImage();
        $tag = $forum->getTags();

        $comment = new Commentaire();
        // $id = $request->get('id');
        $comment->setCree(new \DateTime('now'));
        $forms = $this->createForm(CommentaireType::class, $comment);
        $forms->handleRequest($request);
        $user = $this->getUser();

        if ($forms->isSubmitted() && $forms->isValid()) {

            $comment->setUser($user);
            $comment->setBlog($idf);
            $em->persist($comment);
            $em->flush();
            //return $this->redirectToRoute('Comment_add ');

            $em = $this->getDoctrine()->getManager();

        }

        //$evenement=$em->getRepository()->find($id);
        $mark = $em->getRepository('ForumBundle:Forum')->find($id);
        $Rating = $m->getRepository('ForumBundle:Rating')->AVGRating();
        $Rating=new Rating();




        $form=$this->createFormBuilder($Rating)
            ->add('rating', RatingType::class, [
                'label' => 'Rating'
            ])
            ->add('valider',SubmitType::class, array(
                'attr' => array(

                    'class'=>'btn btn-xs btn-primary'
                )))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $Rating->setSujet($mark->getId());
            $em->persist($Rating);
            $em->flush();
        }

        $commenta = $em->getRepository('ForumBundle:Commentaire')->findAll();



        return $this->render("ForumBundle:Sujet:topic.html.twig", array('comment' => $commenta,'Rating'=>$Rating,'mark' => $mark,'form'=> $form->createView(), 'forms' => $forms->createView(), "id" => $id, "titre" => $titre, "Cree" => $cree, "auteur" => $auteur, "blog" => $blog, "image" => $image, "tag" => $tag));


    }
    public function ShowownAction(Request $request)
    {
        $m = $this->getDoctrine()->getManager();
        $dql   = "SELECT a FROM ForumBundle:Forum a";
        $forum=$m->createQuery($dql);
        /**
         * @var  $paginator \Knp\Component\Pager\Paginator
         **/
        $paginator  = $this->get('knp_paginator');
        $result= $paginator->paginate(
            $forum,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 2)
        );

        $Rating = $m->getRepository('ForumBundle:Rating')->AVGRating();
        return $this->render("@Forum/Sujet/Show_topic_tag.html.twig", array('forum' => $result,'rating' => $Rating));

    }


    public function SignalerAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository('ForumBundle:Forum')->find($id);

        $forum->setSigne($forum->getSigne()+1);
        if($forum->getSigne()>3)
        {
            $forum->setFenabled(true);
        }
        $em->persist($forum);
        $em->flush();
        return $this->redirectToRoute('forum_show');
    }
    public function allAction()
    {
        $tasks=$this->getDoctrine()->getManager()->getRepository('ForumBundle:Forum')->findAll();
        $serializer= new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($tasks);
        return new JsonResponse($formatted);

    }

    public function LoginAction($username,$password)
    {
        $tasks=$this->getDoctrine()->getManager()->getRepository('UserBundle:User')->findBy($username,$password);
        $serializer= new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($tasks);
        return new JsonResponse($formatted);

    }
    public function AddForumAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $forum=new Forum();
        $forum->setTitre($request->get('titre'));
        $forum->setAuteur($request->get('auteur'));
        $forum->setBlog($request->get('blog'));
        $forum->setTags($request->get('tags'));

        $forum->setSigne(0);
        $forum->setCree(new \DateTime('now'));
        //$forum->setCree($request->get('tags'));
        $em->persist($forum);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($forum);
        return new JsonResponse($formatted);
    }
    public function LikesAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $likes=$em->getRepository("ForumBundle:Likes")->findBy(['blog' =>$request->get('blog')]);
		$array=array();
		$i=0;
		foreach($likes as $said)
        {
            array_push($array,$said);
			$i=$i+1;

		}
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($i);
        return new JsonResponse($formatted);
    }

    public function mineAction(Request $request)
    {
        $tasks=$this->getDoctrine()->getManager()->getRepository("ForumBundle:Forum")->findBy(['auteur' =>$request->get('auteur')]);
        $serializer= new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($tasks);
        return new JsonResponse($formatted);

    }
    public function deleteMAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $forum=$em->getRepository("ForumBundle:Forum")->findOneBy(['id'=>$request->get('id')]);

        //$forum->setCree($request->get('tags'));
        $em->remove($forum);
       // $em->persist($forum);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($forum);
        return new JsonResponse($formatted);

    }
    public function ModifierAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $forum=$em->getRepository("ForumBundle:Forum")->findOneBy(['id'=>$request->get('id')]);
        $forum->setModifiee(new \DateTime('now'));
        $forum->setTitre($request->get('titre'));

        $forum->setBlog($request->get('blog'));
        $forum->setTags($request->get('tags'));
        //$forum->setCree($request->get('tags'));
        $em->persist($forum);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($forum);
        return new JsonResponse($formatted);

    }

}

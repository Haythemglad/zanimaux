<?php

namespace ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="User", type="string", length=255,nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="Commentaire", type="string", length=255,nullable=true)
     */
    private $commentaire;


    /**
     * @var integer
     *
     * @ORM\Column(name="blog", type="integer",nullable=true)
     */
    private $blog;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Cree", type="datetime",nullable=true)
     */
    private $cree;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Modifiee", type="datetime",nullable=true)
     */
    private $modifiee;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Commentaire
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set blog
     *
     * @param string $blog
     *
     * @return Commentaire
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;

        return $this;
    }

    /**
     * Get blog
     *
     * @return string
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Set cree
     *
     * @param \DateTime $cree
     *
     * @return Commentaire
     */
    public function setCree($cree)
    {
        $this->cree = $cree;

        return $this;
    }

    /**
     * Get cree
     *
     * @return \DateTime
     */
    public function getCree()
    {
        return $this->cree;
    }

    /**
     * Set modifiee
     *
     * @param \DateTime $modifiee
     *
     * @return Commentaire
     */
    public function setModifiee($modifiee)
    {
        $this->modifiee = $modifiee;

        return $this;
    }

    /**
     * Get modifiee
     *
     * @return \DateTime
     */
    public function getModifiee()
    {
        return $this->modifiee;
    }
}


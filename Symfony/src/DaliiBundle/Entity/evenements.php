<?php

namespace DaliiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * evenements
 *
 * @ORM\Table(name="evenements")
 * @ORM\Entity(repositoryClass="DaliiBundle\Repository\evenementsRepository")
 */
class evenements
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="nbrePlaceDispo", type="integer")
     *
     *
     */
    private $nbrePlaceDispo;
    /**
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    private $estReserve;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return evenements
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return evenements
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return evenements
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     *
     * @return evenements
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="monImage", type="string", length=255)
     */
    public $monImage;
    /**
     * @Assert\File(maxSize="60000000")
     */
    public $file;
    /**
     * Get id
     *
     * @return int
     */
    public function getWebPath()
    {
        return null === $this->monImage ? null : $this->getUploadDir().'/'.$this->monImage;
    }
    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'../../../../web/'.$this->getUploadDir();
    }
    public function getUploadDir()
    {
        // get rid of the _DIR_ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'Pictures';
    }
    public function UploadProfilePicture()
    {

        $this->file->move($this->getUploadRootDir(),$this->file->getClientOriginalName());
        $this->monImage=$this->file->getClientOriginalName();
        $this->file=null;

    }


    /**
     * Get monImage
     * @return string
     */
    public function getMonImage()
    {
        return $this->monImage;
    }
    /**

     * Set monImage
     * @return Category
     * @param string $monImage
     */
    public function setMonImage($monImage)
    {
        $this->monImage = $monImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getEstReserve()
    {
        return $this->estReserve;
    }

    /**
     * @return nbrePlaceDispo
     */
    public function getNbrePlaceDispo()
    {
        return $this->nbrePlaceDispo;
    }

    /**
     * @param nbrePlaceDispo $nbrePlaceDispo
     */
    public function setNbrePlaceDispo($nbrePlaceDispo)
    {
        $this->nbrePlaceDispo = $nbrePlaceDispo;
    }

    /**
     * @param mixed $estReserve
     */
    public function setEstReserve($estReserve)
    {
        $this->estReserve = $estReserve;
    }



}


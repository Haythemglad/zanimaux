<?php

namespace DaliiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * participer
 *
 * @ORM\Table(name="participer")
 * @ORM\Entity(repositoryClass="DaliiBundle\Repository\participerRepository")
 */
class participer

{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *
     ** @ORM\ManyToOne(targetEntity="evenements", inversedBy="participer")
     * @ORM\JoinColumn(name="idevent",referencedColumnName="id",onDelete="CASCADE")
     *
     */
    private $event;

    /**
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="participer")
     *@ORM\JoinColumn(name="idclient",referencedColumnName="id",onDelete="CASCADE")
     *
     */
    private $client;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event.
     *
     * @param string|null $event
     *
     * @return participer
     */
    public function setEvent($event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return string|null
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set client.
     *
     * @param string $client
     *
     * @return participer
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }
}

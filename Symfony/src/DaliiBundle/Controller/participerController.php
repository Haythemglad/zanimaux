<?php

namespace DaliiBundle\Controller;


use DaliiBundle\Entity\participer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class participerController extends Controller
{
    public function participerAction(Request $request)
    {
        if (is_object($this->getUser())) {
            $currentEventId = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $User = $this->getUser();
            $evenement = $em->getRepository('DaliiBundle:evenements')->find($currentEventId);
            $evenement->setNbrePlaceDispo($evenement->getNbrePlaceDispo() - 1);
            $p = new participer();
            $p->setClient($User);
            $p->setEvent($evenement);
            $em->persist($p);
            $em->persist($evenement);
            $em->flush();
            return new JsonResponse(array("id" => $currentEventId));
        }
        return $this->redirectToRoute('user_homepage');
    }

    public function annulerAction(Request $request)
    {
        $currentEventId = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $participer = $em->getRepository('DaliiBundle:participer')->findOneBy(array('client' => $this->getUser() ,'event' => $currentEventId));

        $evenement = $em->getRepository('DaliiBundle:evenements')->find($currentEventId);
        $evenement->setNbrePlaceDispo($evenement->getNbrePlaceDispo() + 1);
        $em->remove($participer);
        $em->flush();
        return new JsonResponse(array("id" => $currentEventId,"nbrePlaceDispo" => $evenement->getNbrePlaceDispo()));
    }
}
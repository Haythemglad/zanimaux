<?php

namespace DaliiBundle\Controller;

use DaliiBundle\Entity\evenements;
use DaliiBundle\Form\evenementsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class eventController extends Controller
{

    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        //$evenements = $em->getRepository('DaliiBundle:evenements')->findAll();
        $evenements = $em->getRepository('DaliiBundle:evenements')->selectLastN(4);
        $allevents = $em->getRepository('DaliiBundle:evenements')->findAll();
        foreach ($evenements as $event) {
            $participer = $em->getRepository('DaliiBundle:participer')->findOneBy(array('client' => $this->getUser(), 'event' => $event->getId()));
            if ($participer != null) {
                $event->setEstReserve(true);
            } else {
                $event->setEstReserve(false);
            }
        }
        return $this->render('DaliiBundle:Events:index.html.twig', array(
            'evenements' => $evenements,
            'allevents' => $allevents
        ));

    }

    public function myEventsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $id = $this->getUser()->getId(); // Change it to user id

        $evenements = $em->getRepository('DaliiBundle:evenements')->findBy(array('user' => $id));

        return $this->render('DaliiBundle:Events:myevents.html.twig', array(
            'evenements' => $evenements,
        ));
    }

    public function addAction(Request $request)
    {
        $event = new evenements();

        $Form = $this->createForm(evenementsType::class, $event);
        $Form->handleRequest($request);
        $user = $this->getUser();

        if ($Form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $event->UploadProfilePicture();
            $event->setUser($user);
            $em->persist($event);
            $em->flush();
            return $this->redirectToRoute('dalii_myevents');


        }

        return $this->render("DaliiBundle:Events:add.html.twig",
            array('form' => $Form->createView()));

    }

    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $currentEventId = $request->get('id');

        $event=$em->getRepository('DaliiBundle:evenements')->find($currentEventId);
        $Form = $this->createForm(evenementsType::class, $event);
        $Form->handleRequest($request);
        $user = $this->getUser();

        if ($Form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $event->UploadProfilePicture();
            $em->persist($event);
            $em->flush();
            return $this->redirectToRoute('dalii_myevents');


        }

        return $this->render("DaliiBundle:Events:update.html.twig",
            array('form' => $Form->createView()));

    }
    public function deleteAction(Request $request)
     {

         $currentEventId = $request->get('id');
         $em = $this->getDoctrine()->getManager();
         $user = $this->getUser();
         $event = $em->getRepository('DaliiBundle:evenements')->find($currentEventId);

         $em->remove($event);
         $em->flush();
         return $this->redirectToRoute('dalii_myevents');
     }
public function adeleteAction(Request $request)
     {

         $currentEventId = $request->get('id');
         $em = $this->getDoctrine()->getManager();
         $user = $this->getUser();
         $event = $em->getRepository('DaliiBundle:evenements')->find($currentEventId);

         $em->remove($event);
         $em->flush();
         return $this->redirectToRoute('dalii_show1');
     }

    public function calendarAction()
    {
        $em = $this->getDoctrine()->getManager();

        $evenements = $em->getRepository('DaliiBundle:evenements')->findAll();


        return $this->render('DaliiBundle:Events:calendar.html.twig', array(
            'evenements' => $evenements,
            $this->loadCalendrierDataAction()

        ));


    }

    public function loadCalendrierDataAction()
    {

        $em = $this->getDoctrine()->getManager();
        //Liste des evenements
        $listeEvents = $em->getRepository('DaliiBundle:evenements')->findAll();
        $listEventsJson = array();
        foreach ($listeEvents as $event)
            $listEventsJson[] = array(
                'title' => $event->getTitle(),
                'start' => "" . ($event->getStartDate()->format('Y-m-d H:i:s')) . "",
                'id' => "" . $event->getId() . ""
            );

        return new JsonResponse ($listEventsJson);


    }

    public function SearchByDateVolsDashboardAction(Request $request)
    {
        $date1 = $request->get('date1');
        $date2 = $request->get('date2');


        $evenements = new evenements();
        $em = $this->getDoctrine()->getManager();
        $vols = $em->getRepository('DaliiBundle:evenements')->searchEvenementByDate($date1, $date2);
        return $this->render('DaliiBundle:evenement:ClientEvenement2.html.twig',
            array("evenement" => $evenement));
    }


    public function findDQLAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository('DaliiBundle:evenements')->findAll();
        $title = $request->get('title');
        if ($request->isXmlHttpRequest()) {
            $serializer = new Serializer(array(new ObjectNormalizer()));
            $liste = $em->getRepository('DaliiBundle:evenements')->select($title);
            $data = $serializer->normalize($liste);
            return new JsonResponse($data);
        }
        return $this->render('DaliiBundle:Events:rechercherec.html.twig', array('l' => $liste));
    }

    public function showRecAction()
    {
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository('DaliiBundle:evenements')->findAll();
        return $this->render('DaliiBundle:Events:showRec.html.twig', array('l' => $liste));
    }

    public function moreAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $evenements = $em->getRepository('DaliiBundle:evenements')->findBy(array('id' => $id));


        return $this->render('DaliiBundle:Events:more.html.twig', array(
            'evenements' => $evenements,

        ));

    }

}

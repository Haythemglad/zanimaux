<?php

namespace DaliiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DaliiBundle:Events:index.html.twig');
    }
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $evenements = $em->getRepository('DaliiBundle:evenements')->findAll();

        return $this->render('evenements/index.html.twig', array(
            'evenements' => $evenements,
        ));
    }
}

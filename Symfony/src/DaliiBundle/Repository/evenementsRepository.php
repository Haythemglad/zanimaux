<?php

namespace DaliiBundle\Repository;

/**
 * evenementsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class evenementsRepository extends \Doctrine\ORM\EntityRepository
{
    public function select($Title)
    {
        $query = $this->getEntityManager()->createQuery
        ("SELECT a FROM DaliiBundle:evenements a WHERE a.$Title  LIKE :Titre ")
            ->setParameter('Titre',$Title);
        return $query->getResult();
    }

    public function selectLastN($n){

        $query = $this->getEntityManager()->createQuery
        ("SELECT event  FROM DaliiBundle:evenements event  ")
           // ->orderBy('event.startDate', 'DESC');
            ->setMaxResults($n);

        return $query->getResult();
    }



}

<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @Notifiable(name="user")
 */
class User extends BaseUser implements NotifiableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length = 255,nullable = true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length = 255,nullable = true)
     */
    protected $prenom;

    /**
     * @var integer
     *
     * @ORM\Column(name="numtel", type="integer", nullable = true)
     */
    protected $numtel;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length = 255,nullable = true)
     */
    protected $habitation;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length = 255,nullable = true)
     */
    protected $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length = 255,nullable = true)
     */
    protected $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length = 255,nullable = true)
     */
    protected $role;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fenable", type="boolean")
     */
    private $fenable=false;

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set habitation
     *
     * @param string $habitation
     *
     * @return User
     */
    public function setHabitation($habitation)
    {
        $this->habitation = $habitation;

        return $this;
    }

    /**
     * Get habitation
     *
     * @return string
     */
    public function getHabitation()
    {
        return $this->habitation;
    }



    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return User
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return User
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }




    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }





    /**
     * Set numtel
     *
     * @param string $numtel
     *
     * @return User
     */
    public function setNumtel($numtel)
    {
        $this->numtel = $numtel;

        return $this;
    }

    /**
     * Get numtel
     *
     * @return string
     */
    public function getNumtel()
    {
        return $this->numtel;
    }

    /**
     * Set fenable
     *
     * @param boolean $fenable
     *
     * @return User
     */
    public function setFenable($fenable)
    {
        $this->fenable = $fenable;

        return $this;
    }

    /**
     * Get fenable
     *
     * @return boolean
     */
    public function getFenable()
    {
        return $this->fenable;
    }
}

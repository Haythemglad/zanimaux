<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }


    public function loginMobileAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository("UserBundle:User")->findOneBy(['username' =>$request->get('username')]);
        if($user){
            $factory = $this->get('security.encoder_factory');


            $encoder = $factory->getEncoder($user);

            $salt = $user->getSalt();

            if($encoder->isPasswordValid($user->getPassword(),$request->get('password'), $salt)||$user->getPassword()==$request->get('password')){
                $serializer=new Serializer([new ObjectNormalizer()]);
                $formatted=$serializer->normalize($user);
                return new JsonResponse($formatted);
            }
        }
        return new JsonResponse("Failed");
    }

    public function SigninAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $user=new User();
        $user->setUsername($request->get('username'));

        $user->setEmail($request->get('email'));
        $user->setPassword($request->get('password'));
        $user->setNom($request->get('nom'));
        $user->setPrenom($request->get('prenom'));
        $user->setActivationcode($request->get('Activationcode'));
        $user->setNumtel($request->get('numtel'));
        $user->setEnab(0);
        $em->persist($user);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($user);
        return new JsonResponse($formatted);
    }


    public function ActivateAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository("UserBundle:User")->findOneBy(['id' =>$request->get('id')]);

        $user->setEnab(1);

        //$forum->setCree($request->get('tags'));
        $em->persist($user);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($user);
        return new JsonResponse($formatted);

    }
}

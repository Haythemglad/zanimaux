<?php

namespace ServicesBundle\Repository;

/**
 * favorisRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class favorisRepository extends \Doctrine\ORM\EntityRepository
{
    function findBywholiked($wholiked)
    {

        $query=$this->getEntityManager()->createQuery("SELECT f FROM ServicesBundle:favoris f JOIN ServicesBundle:services s WHERE f.wholiked= :u AND f.service=s.id")->setParameter('u',$wholiked);

        return $query->getResult();
    }


}

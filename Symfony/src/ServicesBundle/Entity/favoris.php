<?php

namespace ServicesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * favoris
 *
 * @ORM\Table(name="favoris")
 * @ORM\Entity(repositoryClass="ServicesBundle\Repository\favorisRepository")
 */
class favoris
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ServicesBundle\Entity\services",inversedBy="id")
     *
     * @ORM\JoinColumn(name="service", referencedColumnName=",nullable=true)
     */
    private $service;

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User",inversedBy="id")
     *
     * @ORM\JoinColumn(name="wholiked", referencedColumnName=",nullable=true)
     */
    private $wholiked;

    /**
     * @return mixed
     */
    public function getWholiked()
    {
        return $this->wholiked;
    }

    /**
     * @param mixed $wholiked
     */
    public function setWholiked($wholiked)
    {
        $this->wholiked = $wholiked;
    }


}


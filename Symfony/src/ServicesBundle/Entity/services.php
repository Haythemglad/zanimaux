<?php

namespace ServicesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * services
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="ServicesBundle\Repository\servicesRepository")
 */
class services
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=true)
     */
    private $role;


    /**
     * @var string
     *
     * @ORM\Column(name="laltitude", type="float", length=255, nullable=true)
     */
    private $laltitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="float", length=255, nullable=true)
     */
    private $longitude;

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLaltitude()
    {
        return $this->laltitude;
    }

    /**
     * @param string $laltitude
     */
    public function setLaltitude($laltitude)
    {
        $this->laltitude = $laltitude;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createDt", type="datetime", nullable=true)
     */
    private $createDt;


    /**
     * @var string
     *
     * @ORM\Column(name="favori", type="boolean", length=255, nullable=true)
     */
    private $favori;

    /**
     * @return string
     */
    public function getFavori()
    {
        return $this->favori;
    }

    /**
     * @param string $favori
     */
    public function setFavori($favori)
    {
        $this->favori = $favori;
    }




    /**
     * @var string
     *
     * @ORM\Column(name="adresseS", type="string", length=255, nullable=true)
     */
    private $adresseS;


    /**
     * @var string
     *
     * @ORM\Column(name="numtel", type="integer", length=255, nullable=true)
     */
    private $numtel;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    public $image;
    /**
     * @Assert\File(maxSize="60000000")
     */
    public $file;
    /**
     * Get id
     *
     * @return int
     */
    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }
    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'../../../../web/'.$this->getUploadDir();
    }
    public function getUploadDir()
    {
        // get rid of the _DIR_ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'Pictures';
    }
    public function UploadProfilePicture()
    {
$ur = $this->getUploadRootDir();
$cn=$this->file->getClientOriginalName();
        $this->file->move($ur,$cn);
        $this->image=$this->file->getClientOriginalName();
        $this->file=null;

    }


    /**
     * Get image
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    /**

     * Set monImage
     * @return Category
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * @return string
     */
    public function getNumtel()
    {
        return $this->numtel;
    }

    /**
     * @param string $numtel
     */
    public function setNumtel($numtel)
    {
        $this->numtel = $numtel;
    }

    /**
     * @return string
     */
    public function getAdresseS()
    {
        return $this->adresseS;
    }

    /**
     * @param string $adresseS
     */
    public function setAdresseS($adresseS)
    {
        $this->adresseS = $adresseS;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="userservice", type="string", length=255, nullable=true )
     */
    private $userservice;


    /**
     * @return mixed
     */
    public function getUserservice()
    {
        return $this->userservice;
    }

    /**
     * @param mixed $userservice
     */
    public function setUserservice($userservice)
    {
        $this->userservice = $userservice;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ServicesBundle\Entity\categorie",inversedBy="servicecat")
     *
     * @ORM\JoinColumn(name="catservice", referencedColumnName=",nullable=true)
     */
    private $catservice;

    /**
     * @return mixed
     */
    public function getCatservice()
    {
        return $this->catservice;
    }

    /**
     * @param mixed $catservice
     */
    public function setCatservice($catservice)
    {
        $this->catservice = $catservice;
    }





    /**
     * @ORM\OneToMany(targetEntity="ServiceBundle\Entity\rendezVous", mappedBy="service")
     */
    private $renezVous;






    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $renezVous
     */
    public function setRenezVous($renezVous)
    {
        $this->renezVous = $renezVous;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return services
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return services
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDt
     *
     * @param \DateTime $createDt
     *
     * @return services
     */
    public function setCreateDt($createDt)
    {
        $this->createDt = $createDt;

        return $this;
    }

    /**
     * Get createDt
     *
     * @return \DateTime
     */
    public function getCreateDt()
    {
        return $this->createDt;
    }



    /**
     * Set role
     *
     * @param string $role
     *
     * @return services
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }



}

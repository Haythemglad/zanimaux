<?php

namespace ServicesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="ServicesBundle\Repository\categorieRepository")
 */
class categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    public $image;
    /**
     * @Assert\File(maxSize="60000000")
     */
    public $file;
    /**
     * Get id
     *
     * @return int
     */
    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }
    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'../../../../web/'.$this->getUploadDir();
    }
    public function getUploadDir()
    {
        // get rid of the _DIR_ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'Pictures';
    }
    public function UploadProfilePicture()
    {
        $ur = $this->getUploadRootDir();
        $cn=$this->file->getClientOriginalName();
        $this->file->move($ur,$cn);
        $this->image=$this->file->getClientOriginalName();
        $this->file=null;

    }


    /**
     * Get image
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    /**

     * Set monImage
     * @return Category
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="usercat", type="string", length=255, nullable=true )
     */
    private $usercat;

    /**
     * @return mixed
     */
    public function getUsercat()
    {
        return $this->usercat;
    }

    /**
     * @param mixed $usercat
     */
    public function setUsercat($usercat)
    {
        $this->usercat = $usercat;
    }





    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return categorie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @ORM\OneToMany(targetEntity="ServicesBundle\Entity\services", mappedBy="catservice")
     */
    private $servicecat;

    /**
     * @return string
     */
    public function getServicecat()
    {
        return $this->servicecat;
    }

    /**
     * @param string $servicecat
     */
    public function setServicecat($servicecat)
    {
        $this->servicecat = $servicecat;
    }



}


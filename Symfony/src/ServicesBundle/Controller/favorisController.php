<?php

namespace ServicesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class favorisController extends Controller
{
    public function deleteAction(Request $request)
    {

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $favoris = $em->getRepository("ServicesBundle:favoris")->find($id);
        $em->remove($favoris);
        $em->flush();
        return $this->redirectToRoute('servicefavoriuser');

    }


}

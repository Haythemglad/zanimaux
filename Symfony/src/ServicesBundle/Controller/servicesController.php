<?php

namespace ServicesBundle\Controller;

use Doctrine\DBAL\Types\StringType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\HttpFoundation\Request;
use ServicesBundle\Form\servicesType;
use ServicesBundle\Entity\services;
use ServicesBundle\Entity\categorie;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use ServicesBundle\Entity\favoris;


class servicesController extends Controller
{
    public function listAction(Request $request)
    {

        $user=$this->getUser();
        $userservice=$user->getUsername();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findAll();//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );
        $user = $this->getUser()->getRoles();
        if ($request->isMethod('POST'))
        {
            {
                $title = $request->get('title');
                $description = $request->get('description');
                $adresseS=$request->get('adresseS');



                $services = $em->getRepository("ServicesBundle:services")

                    ->findBy(array("title" => $title,"description"=>$description,"adresseS"=>$adresseS));
                $this->redirectToRoute('Servicesuser');
            }
        }
        return $this->render("@Services/services/listservice.html.twig",
            array('services' => $result
            ));



    }

    public function loadEventDataAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->findAll();
        $services = $em->getRepository("ServicesBundle:services")->findByTitleLike($request->get('name'));
        $servicesJson = array();
        foreach ($services as $service){
            $date = '';
            if($service->getCreateDt() != null){
                $date =  $service->getCreateDt()->format("Y/m/d H:m:s");
            }
            $servicesJson[] = array(
                "idService" => $service->getId(),
                "title" => $service->getTitle(),
                "description"=>$service->getDescription(),
                "userservice"=>$service->getUserservice(),
                "catservice"=>$service->getCatservice(),
                "createDt"=>$date,
                "numtel"=>$service->getNumtel(),
                "adresseS"=>$service->getAdresseS(),
                "image"=>$service->getImage(),

            );
        }
        return new JsonResponse(array('listeServices' => $servicesJson));
    }

    public function malistvetAction(Request $request)
    {
        $user=$this->getUser();
        $userservice=$user->getUsername();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServicevet($userservice);//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',3)
        );
        $user = $this->getUser()->getRoles();
        if (in_array("ROLE_VET", $user) == true) {

            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }}
            return $this->render("@Services/services/messervices/malistevet.html.twig",
                array('services' => $result
                ));
        }


    public function listvetAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServicevet();//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );
            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }
            return $this->render("@Services/services/toutlesservices/listevet.html.twig",
                array('services' => $result
                ));
        }





    public function malistgardeAction(Request $request)
    {
        $user=$this->getUser();
        $userservice=$user->getUsername();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServiceGarde($userservice);//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );
        $user = $this->getUser()->getRoles();
        if (in_array("ROLE_DRE", $user) == true) {

                if ($request->isMethod('POST')) {
                    {
                        $title = $request->get('title');
                        $description = $request->get('description');
                        $adresseS = $request->get('adresseS');


                        $services = $em->getRepository("ServicesBundle:services")
                            ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                        $this->redirectToRoute('Servicesuser');
                    }
                }
            }
        return $this->render("@Services/services/messervices/malistegarde.html.twig",
            array('services' => $result
            ));
    }


    public function listgardeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServiceGarde();//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );

            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }
            return $this->render("@Services/services/toutlesservices/listegarde.html.twig",
                array('services' => $result
                ));

    }

    public function malistEdAction(Request $request)
    {
        $user=$this->getUser();
        $userservice=$user->getUsername();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServiceEducationC($userservice);//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );
        $user = $this->getUser()->getRoles();
        if (in_array("ROLE_PET", $user) == true) {

            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }

        }
        return $this->render("@Services/services/messervices/malisteEducat.html.twig",
            array('services' => $result
            ));
    }

    public function listEdAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServiceEducationC();//findByUserservice($userservice);
        /**
         * @var $paginator \knp\Component\Pager\Paginator
         */
        $paginator=$this->get('knp_paginator');
        $result=$paginator->paginate(
            $services,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',9)
        );


            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }
            return $this->render("@Services/services/toutlesservices/listeEducat.html.twig",
                array('services' => $result
                ));
        }



    public function detailsAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->find($id);
        $title = $services->getTitle();
        $description = $services->getDescription();
        $userservice = $services->getUserservice();
        $catservice=$services->getCatservice();
        $createDt = $services->getCreateDt();
        $numtel = $services->getNumtel();
        $adresseS = $services->getAdresseS();
        $image=$services->getImage();
        $laltitude = $services->getLaltitude();
        $longitude = $services->getLongitude();


        return $this->render("ServicesBundle:services:detailsservices.html.twig"
            ,array(
                "id"=>$id,
                "title"=>$title,
                "description"=>$description,
                "userservice"=>$userservice,
                "catservice"=>$catservice,
                "createDt"=>$createDt,
                "numtel"=>$numtel,
                "adresseS"=>$adresseS,
                "image"=>$image,
                "laltitude"=>$laltitude,
                "longitude"=>$longitude
            ));

    }

    public function addAction(Request $request)
    {
        $services = new services();

        $user = $this->getUser();
        $userservice = $user->getUsername();
        $services->setCreateDt(new \DateTime('now'));
        $form = $this->createForm(servicesType::class, $services);
        $form->handleRequest($request);
        if (($form->isValid())) {

            $em = $this->getDoctrine()->getManager();
            $services->setUserservice($userservice);
            $services->UploadProfilePicture();
            $em->persist($services);
            $em->flush();
            return $this->redirectToRoute('Servicesuser');
        }
        return $this->render("ServicesBundle:services:addService.html.twig",
            array('form' => $form->createView()));
    }
    public function updateSAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->find($id);
        $form = $this->createForm(servicesType::class,$services);
        $form->handleRequest($request);
        if (($form->isValid())) {

            $em = $this->getDoctrine()->getManager();
            $services->UploadProfilePicture();
            $em->persist($services);
            $em->flush();
            return $this->redirectToRoute('Servicesuser');
        }
        return $this->render("@Services/services/updateService.html.twig"
            ,array("form"=>$form->createView()));
    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->find($id);
        $em->remove($services);
        $em->flush();
        return $this->redirectToRoute('listecategorieuser');
    }

    public function listfavoriAction(Request $request)
    {
        $wholiked=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByfavori($wholiked);//findByUserservice($userservice);
        $user = $this->getUser()->getRoles();
            if ($request->isMethod('POST')) {
                {
                    $title = $request->get('title');
                    $description = $request->get('description');
                    $adresseS = $request->get('adresseS');


                    $services = $em->getRepository("ServicesBundle:services")
                        ->findBy(array("title" => $title, "description" => $description, "adresseS" => $adresseS));
                    $this->redirectToRoute('Servicesuser');
                }
            }

        return $this->render("@Services/services/favori.html.twig",
            array('services' => $services
            ));
    }
    public function favoriAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->find($request->get('id'));
        //add comme favoris
        $favoris = new favoris();

        $user = $this->getUser();
        $wholiked = $user;

        $favoris->setWholiked($wholiked);
        $favoris->setService($services);
        $em->persist($favoris);
        $em->flush();
        return $this->redirectToRoute('servicefavoriuser');
    }




//unique name image

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }




}

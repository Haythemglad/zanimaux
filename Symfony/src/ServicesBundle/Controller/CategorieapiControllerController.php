<?php

namespace ServicesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CategorieapiControllerController extends Controller
{
    public function listapiAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository("ServicesBundle:categorie")->findAll();//findByUsercat($usercat);
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $categories=$serializer->normalize($categorie);
        return new JsonResponse($categories);

    }

}

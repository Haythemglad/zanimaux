<?php

namespace ServicesBundle\Controller;

use ServicesBundle\Entity\favoris;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class FavorisapiController extends Controller
{



    public function listfavAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $favoris = $em->getRepository("ServicesBundle:favoris")
            ->findBywholiked($this->getUser());

        //$wholiked = $em->getRepository("UserBundle:User")->find($request->get('wholiked'));//findByUserservice($userservice);
        //$service = $em->getRepository("ServicesBundle:services")->find($request->get('service'));
        //$favoris->setService($service);

        $wholiked=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByfavori($wholiked);//findByUserservice($userservice);
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);

        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($favoris);
        return new JsonResponse($service);
    }




    public function deleteAction(Request $request)
    {

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $favoris = $em->getRepository("ServicesBundle:favoris")->find($id);
        $em->remove($favoris);
        $em->flush();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer = new Serializer([$normalizer]);
        $service = $serializer->normalize($favoris);
        return new JsonResponse($service);
    }








}

<?php

namespace ServicesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use ServicesBundle\Entity\services;
use ServicesBundle\Form\servicesType;
use ServicesBundle\Entity\favoris;


class ServiceapiControllerController extends Controller
{

    public function listvetAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServicevet();//findByUserservice($userservice);



        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);
    }


    public function listgardeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServiceGarde();//findByUserservice($userservice);


        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);

    }
    public function listEdAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByServiceEducationC();//findByUserservice($userservice);
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);

    }

    public function detailsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->findBy(array("id" => $request->get("id")));

        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);


    }

    public function addAction(Request $request)
    {
        $services = new services();




            $em = $this->getDoctrine()->getManager();
            $services->setUserservice($request->get("userservice"));
            $services->setAdresseS($request->get("adresseS"));
            $services->setDescription($request->get("description"));
            $services->setTitle($request->get("title"));
            $services->setNumtel($request->get("numtel"));
            $services->setFavori($request->get("favori"));
            $services->setLaltitude($request->get("laltitude"));
            $services->setLongitude($request->get("longitude"));
            $services->setCategorie($request->get("categorie"));
            $services->setImage($request->get("image"));

        $services->setCreateDt(new \DateTime('now'));
        $cat=$em->getRepository("ServicesBundle:categorie")->findBy(array('name'=>$request->get('catservice')));
        $services->setCatservice($cat[0]);
            $em->persist($services);
            $em->flush();



        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);

    }

    public function updateSAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->find($id);
        $services->setAdresseS($request->get("adresseS"));
        $services->setDescription($request->get("description"));
        $services->setTitle($request->get("title"));
        $services->setNumtel($request->get("numtel"));
        $services->setLaltitude($request->get("laltitude"));
        $services->setLongitude($request->get("longitude"));
            $em->persist($services);
            $em->flush();

            $normalizer=new ObjectNormalizer();

            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId(); // Change this to a valid method of your object
            });
            $serializer=new Serializer([$normalizer]);
            $service=$serializer->normalize($services);
            return new JsonResponse($service);

}

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->find($id);
        $em->remove($services);
        $em->flush();
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);
    }

    public function malistvetAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServicevet(array("userservice" => $request->get("userservice")));//findByUserservice($userservice);
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);

    }

    public function malistEdAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServiceEducationC(array("userservice" => $request->get("userservice")));//findByUserservice($userservice);

        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);
    }

    public function malistgardeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByMesServiceGarde(array("userservice" => $request->get("userservice")));//findByUserservice($userservice);
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);
    }

    public function listfavoriAction(Request $request)
    {

        //$cat=$em->getRepository("ServicesBundle:categorie")->findBy(array('name'=>$request->get('catservice')));
        //$services->setCatservice($cat[0]);
        $wholiked=$this->getUser();
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")->findByfavori($wholiked);//findByUserservice($userservice);
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($service);
    }

    public function favoriAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository("ServicesBundle:services")
            ->find($request->get('id'));
        $user=$em->getRepository("UserBundle:User")
            ->find($request->get('idd'));
        //add comme favoris
        $favoris = new favoris();

        //$user = $this->getUser();
        //$wholiked = $user;

        $favoris->setWholiked($user);
        $favoris->setService($services);
        $em->persist($favoris);
        $em->flush();
        $normalizer=new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId(); // Change this to a valid method of your object
        });
        $serializer=new Serializer([$normalizer]);
        $service=$serializer->normalize($services);
        return new JsonResponse($favoris);
    }





}

<?php

namespace AdminTemplateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forum
 *
 * @ORM\Table(name="forumAdmin")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\ForumRepository")
 */
class ForumAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255 ,nullable=true)
     */
    public $titre;

    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="integer",nullable=true)
     */
    public $rating;
    /**
     * @var string
     *
     * @ORM\Column(name="Auteur", type="string", length=255 ,nullable=true)
     */

    public $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="Blog", type="string", length=255,nullable=true)
     */
    public $blog;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=5000,nullable=true)
     */
    public $image;

    /**
     * @var string
     *
     * @ORM\Column(name="Tags", type="string", length=255,nullable=true)
     */
    public $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaires", type="string", length=255,nullable=true)
     */
    public $commenantaires;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Cree", type="datetime")
     */
    public $cree;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param string $auteur
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    }

    /**
     * @return string
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param string $blog
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getCommenantaires()
    {
        return $this->commenantaires;
    }

    /**
     * @param mixed $commenantaires
     */
    public function setCommenantaires($commenantaires)
    {
        $this->commenantaires = $commenantaires;
    }

    /**
     * @return \DateTime
     */
    public function getCree()
    {
        return $this->cree;
    }

    /**
     * @param \DateTime $cree
     */
    public function setCree($cree)
    {
        $this->cree = $cree;
    }

    /**
     * @return mixed
     */
    public function getModifiee()
    {
        return $this->Modifiee;
    }

    /**
     * @param mixed $Modifiee
     */
    public function setModifiee($Modifiee)
    {
        $this->Modifiee = $Modifiee;
    }

    /**
     * @ORM\Column(type="datetime" ,nullable=true)
     */
    public $Modifiee;
}


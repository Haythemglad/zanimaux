<?php

namespace AdminTemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdminTemplateBundle:Default:index.html.twig');
    }

    public function menuAction(){

        return $this->render('AdminTemplateBundle:Default:menu.html.twig');
    }

    public function AnnoncesAction()
    {
        return $this->render('AdminTemplateBundle:Template:Annonces.html.twig');
    }

}

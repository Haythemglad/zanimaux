<?php

namespace AdminTemplateBundle\Controller;

use AdminTemplateBundle\Entity\Forum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ForumadminController extends Controller
{



    public function loadForumDataTablesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $forumList = $em->getRepository('ForumBundle:Forum')->findAll();

        return $this->render('AdminTemplateBundle:Template:Forum.html.twig',array('m'=>$forumList));

    }


    public function loadForumDataUserTablesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $forumList = $em->getRepository('UserBundle:User')->findAll();

        return $this->render('AdminTemplateBundle:Template:ForumAdmin.html.twig',array('m'=>$forumList));

    }




    public function showRecAction()
    {
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository('GestionReclamationBundle:Reclamation')->findAll();
        return $this->render('AdminTemplateBundle:Template:showRec.html.twig', array('l' => $liste));
    }
    public function banirAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository('UserBundle:User')->find($id);

        $forum->setFenable(true);

        $em->persist($forum);
        $em->flush();
        return $this->redirectToRoute('forum_show');
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicess;

import Entitys.Reclamation;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mehdi
 */
public class ServiceRec {
    
    
    
    
     public ArrayList<Reclamation> getallReclamation() {
        ArrayList<Reclamation> listannonce = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/mobile/reclamation/get/33");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> reclamation= jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(reclamation);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) reclamation.get("listeReclamations");
                    
                    for (Map<String, Object> obj : list) {
                       Reclamation a = new Reclamation(); 
                        
                       
                    
                       //float prix = Float.parseFloat(obj.get("Prix").toString());
                     // r.setId((int) id);
                        
                        a.setCategorie(obj.get("categorie").toString());
                        a.setTitre(obj.get("titre").toString());
                        a.setDescription(obj.get("description").toString());
                     //a.setDateCreation(obj.get("dateCreation").toString());
                     
                       // a.setDateCreation(obj.get("dateCreation").toString());
                        a.setStatut(obj.get("statut").toString());
                        a.setReponse(obj.get("reponse").toString());
                       // a.setDescription(obj.get("Description").toString());
                       // a.setPrix(prix);
                       // a.setIobj.get("Image").toString());
                        
                        System.out.println("catego"+obj.get("categorie").toString()); 
                        
                        listannonce.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listannonce;
    }
     
     
    public void AddReclamation(Reclamation r) {
        
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/Animaux/web/app_dev.php/mobile/reclamation/add/"+ r.getCategorie() +"/"+ r.getTitre() +"/"+ r.getDescription() +"/"+LoggedUser.getId();
        
        
        
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
            
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
     
}

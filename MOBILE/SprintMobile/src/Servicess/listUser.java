/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicess;

import com.mycompagny.Entite.User;
import Entitys.Reclamation;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mehdi
 */
public class listUser {

    public ArrayList<User> getAllUser() {

        ArrayList<User> listUser = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/mobile/user/getAll");
        System.out.println(con.getUrl());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> user = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(user);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) user.get("listUser");

                    for (Map<String, Object> obj : list) {
                        User a = new User();

                        a.setUsername(obj.get("username").toString());

                        System.out.println("Username" + obj.get("username").toString());

                        listUser.add(a);
                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listUser;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicess;
import Entitys.Annonce;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hamouda
 */
public class CRUDD {
    
    public ArrayList<Annonce> getallAnnonce() {
        ArrayList<Annonce> listannonce = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/annonce/AffichageMobile");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> annonces = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(annonces);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) annonces.get("listAnnonce");
                    for (Map<String, Object> obj : list) {
                        Annonce a = new Annonce();
                        
                       float prix = Float.parseFloat(obj.get("Prix").toString());
                        float id = Float.parseFloat(obj.get("Id").toString());
                        a.setId((int) id);
                        float user_id = Float.parseFloat(obj.get("User").toString());
                        
                        a.setUser_id((int)user_id);
                        a.setCategorie(obj.get("Categorie").toString());
                        a.setAnimal(obj.get("Animal").toString());
                        a.setRace(obj.get("Race").toString());
                        a.setDescription(obj.get("Description").toString());
                        a.setPrix(prix);
                        a.setImage(obj.get("Image").toString());
                        
                        
                        
                        listannonce.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listannonce;
    }
    public ArrayList<Annonce> getallAnnoncee() {
         ArrayList<Annonce> listannonce = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/annonce/Affichagee/"+LoggedUser.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> annonces = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(annonces);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) annonces.get("listAnnonce");
                    for (Map<String, Object> obj : list) {
                        Annonce a = new Annonce();
                        
                       float id = Float.parseFloat(obj.get("Id").toString());
                       float prix = Float.parseFloat(obj.get("Prix").toString());
                         a.setId((int) id);
                          float user_id = Float.parseFloat(obj.get("User").toString());
                        
                        a.setUser_id((int)user_id);
                         
                        
                        a.setCategorie(obj.get("Categorie").toString());
                        a.setAnimal(obj.get("Animal").toString());
                        a.setRace(obj.get("Race").toString());
                        a.setDescription(obj.get("Description").toString());
                        a.setPrix(prix);
                        a.setImage(obj.get("Image").toString());
                        
                        
                        
                        listannonce.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listannonce;
       
    }
    public ArrayList<Annonce> getallAnnonce1() {
        ArrayList<Annonce> listannonce = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/annonce/AffichageMobile1");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> annonces = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(annonces);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) annonces.get("listAnnonce");
                    for (Map<String, Object> obj : list) {
                        Annonce a = new Annonce();
                        
                       //float prix = Float.parseFloat(obj.get("Prix").toString());
                     // r.setId((int) id);
                     float id = Float.parseFloat(obj.get("Id").toString());
                      // float prix = Float.parseFloat(obj.get("Prix").toString());
                         a.setId((int) id);
                          float user_id = Float.parseFloat(obj.get("User").toString());
                        
                        a.setUser_id((int)user_id);
                        
                        a.setCategorie(obj.get("Categorie").toString());
                        a.setAnimal(obj.get("Animal").toString());
                        a.setRace(obj.get("Race").toString());
                        a.setDescription(obj.get("Description").toString());
                       // a.setPrix(prix);
                        a.setImage(obj.get("Image").toString());
                        
                        
                        
                        listannonce.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listannonce;
    }
     public ArrayList<Annonce> getallAnnonce2() {
        ArrayList<Annonce> listannonce = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/annonce/AffichageMobile2");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> annonces = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(annonces);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) annonces.get("listAnnonce");
                    for (Map<String, Object> obj : list) {
                        Annonce a = new Annonce();
                        
                       //float prix = Float.parseFloat(obj.get("Prix").toString());
                     // r.setId((int) id);
                        
                        a.setCategorie(obj.get("Categorie").toString());
                        a.setAnimal(obj.get("Animal").toString());
                        a.setRace(obj.get("Race").toString());
                        a.setDescription(obj.get("Description").toString());
                       // a.setPrix(prix);
                        a.setImage(obj.get("Image").toString());
                        
                        
                        
                        listannonce.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listannonce;
    }
      public void ajout_annonce (Annonce a){
         ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/annonce/Ajouter/"+LoggedUser.getId()+"?&categorie=" + a.getCategorie()+"&animal="+ a.getAnimal()+"&race="+ a.getRace()+"&description="+ a.getDescription()+ "&prix=" + a.getPrix()+"&image=" + a.getImage();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
         
         public void modifier_annonce (Annonce a ){
         ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/annonce/modifier/"+a.getId()+"?&categorie=" + a.getCategorie()+"&animal="+ a.getAnimal()+"&race="+ a.getRace()+"&description="+ a.getDescription()+ "&prix=" + a.getPrix();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
         public void annuler_annonce (Annonce a){
             ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/annonce/Supprimer/"+a.getId();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;






/**
 *
 * @author haythem
 */
public class Rendez_vous {
    
    
    private int Id;
    private String message;
    private String date;
    private String heure;
    private int demandeur_id;
    private int valide;
    private int accepteur_id;
    private int remove;
    private String service;
    private String email_demandeur;
    private String email_accepteur;
    

    public Rendez_vous() {
        
    }

    public Rendez_vous(int Id) {
        this.Id = Id;
    }
    
    
    
    
    public Rendez_vous(String message, String date, String heure, String service) {
        this.message = message;
        this.date = date;
        this.heure = heure;
        this.service = service;
    }
    
   public Rendez_vous(int Id, String date, String heure) {
        
        this.Id = Id;
        this.date = date;
        this.heure = heure;
      
        
    }

    public Rendez_vous(String message, String date, String heure, int demandeur_id, int valide, int accepteur_id, int remove, String service) {
        
        this.message = message;
        this.date = date;
        this.heure = heure;
        this.demandeur_id = demandeur_id;
        this.valide = valide;
        this.accepteur_id = accepteur_id;
        this.remove = remove;
        this.service = service;
        
    }



   
    
    

  

    @Override
    public String toString() {
        return "Rendez_vous{" + "message=" + message + ", date=" + date + ", heure=" + heure + ", service=" + service + '}';
    }

    


    
    

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }



    public int getDemandeur_id() {
        return demandeur_id;
    }

    public void setDemandeur_id(int demandeur_id) {
        this.demandeur_id = demandeur_id;
    }

    

    public int getAccepteur_id() {
        return accepteur_id;
    }

    public void setAccepteur_id(int accepteur_id) {
        this.accepteur_id = accepteur_id;
    }

    

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getValide() {
        return valide;
    }

    public void setValide(int valide) {
        this.valide = valide;
    }

    public int getRemove() {
        return remove;
    }

    public void setRemove(int remove) {
        this.remove = remove;
    }

    public String getEmail_demandeur() {
        return email_demandeur;
    }

    public void setEmail_demandeur(String email_demandeur) {
        this.email_demandeur = email_demandeur;
    }

    public String getEmail_accepteur() {
        return email_accepteur;
    }

    public void setEmail_accepteur(String email_accepteur) {
        this.email_accepteur = email_accepteur;
    }

    
    
}

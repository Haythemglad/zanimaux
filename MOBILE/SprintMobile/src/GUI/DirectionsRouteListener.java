/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

/**
 *
 * @author shannah
 */
public interface DirectionsRouteListener {
    
    public void routeCalculated(DirectionsResult result);
    
}

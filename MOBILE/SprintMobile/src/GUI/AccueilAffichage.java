/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import GUIs.Acceuil;
import GUIs.Home;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.uikit.cleanmodern.AffichageCategorie;
import com.codename1.uikit.cleanmodern.AffichageForum;
import com.codename1.uikit.cleanmodern.BaseForm;
import com.codename1.uikit.cleanmodern.SignInForm;


/**
 *
 * @author haythem
 */
public class AccueilAffichage extends BaseForm {
    
    public Form hi ;
     public Resources theme;
     public Resources res;
     Container c;
    
    public AccueilAffichage(Resources res){
        
        theme = UIManager.initFirstTheme("/theme");
        
        c = new Container(new FlowLayout(Component.CENTER)); 
        
        
        Image a = theme.getImage("signin-background.jpg");
        
        c.getAllStyles().setBgImage(a);
        
       hi = new Form("Accueil", new FlowLayout(Component.CENTER));
    
        Button evenemments = new Button("Gestion des évenemments");
        Button services = new Button("Gestion des services");
        Button forum = new Button("Gestion des forum");
        Button reclamations = new Button("Gestion des réclamations");
        Button annonces = new Button("Gestion des annonces");
        Button rdvs = new Button("Gestion des rendez-vous");
        Button deco = new Button("Deconnexion");
        
        
        
        
        
        
        
        c.add(services);
        c.add(forum);
        c.add(reclamations);
        c.add(annonces);
        c.add(rdvs);
        c.add(evenemments);
        c.add(deco);
      
        
        
        
        
        evenemments.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                AccueilAffichage a = new AccueilAffichage(res);
                a.getF().show();
            }
        });
        
        
        reclamations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                Acceuil a=new Acceuil();
                a.getHome().show();
            }
        });
        
        
        
        
        
        
 
        
        annonces.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                Home a=new Home();
                a.getHome().show();
            }
        });
        
        rdvs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                GestionrdvAffichage a=new GestionrdvAffichage();
                a.getF().show();
            }
        });
        
        deco.addActionListener((e)->{
        new SignInForm(res).show();
        
        
        });
        
        forum.addActionListener((e)->{
        new AffichageForum(res).show();
        
        
        });
        
        services.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
               new AffichageCategorie(res).show();
            }
        });
        
        
        
        
        
        hi.add(c);
        
        
        
    hi.show();
}
    
    public Form getF() {
        
        

        return hi;
    }
    
}

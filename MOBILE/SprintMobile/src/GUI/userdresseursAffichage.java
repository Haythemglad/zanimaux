/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.Rendez_vous;
import com.mycompagny.Entite.User;
import Services.CRUD;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.DateFormat;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.BorderLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import Services.Sqllight; 
import com.codename1.components.ToastBar;
import com.codename1.googlemaps.MapContainer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.messaging.Message;
import com.codename1.ui.Display;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author haythem
 */
public class userdresseursAffichage {
    
    Form f,f1;
    SpanLabel lb,lb1;
    Button prendrerdv,details,btn_map,backmap,back,backk;
  Container y,j,k,o,ca;
  Container y1,j1,k1,o1;
  ArrayList<User> lis;
  public String resultat = "";
  public String d = "";
  public String heure = "";
  public String datejourr,daterendezvouss;
  
  private static final String HTML_API_KEY = "AIzaSyAi6eh0HqBK-VL_NL27RsXRr8eh0DBpRU0";
    private Form current;
          
          
          
  public int idd;
  
  ArrayList<Rendez_vous> listerdv;
  
  public Resources theme;
  private EncodedImage enc ;
    
    
    String URL ="http://localhost/Animaux/web/Picture/";
  
    public userdresseursAffichage() {
        
        Sqllight s = new Sqllight();
        
        theme = UIManager.initFirstTheme("/theme");
        
        Image a = theme.getImage("signin-background.jpg");
        
        
        f = new Form("Liste des dresseurs", new FlowLayout(Component.CENTER, Component.CENTER));
        CRUD c =new CRUD();
        lis = new ArrayList<User>();
         lis=c.getdresseurs();
        y= new Container(BoxLayout.y());
        y1 = new Container(BoxLayout.y());
        
        y.getAllStyles().setBgImage(a);
        
         back = new Button("back");
        
       y.add(back);
       
       
        
       
        
           for (User li : lis) {
               
              j= new Container(BoxLayout.x());
              k = new Container(BoxLayout.y());
              o = new Container(BoxLayout.x());
              
                
              prendrerdv = new Button("Prendre un rendez-vous");
              
              details = new Button("Voir détails");
              
              
               btn_map = new Button("Destination");
          
          btn_map.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               
               Location position = LocationManager.getLocationManager().getCurrentLocationSync();
        System.out.println(position.getLatitude() + "    " + position.getLongitude());

        //////////////////////////// Declaration et parametre de base ////////////////////////////////////
        final MapContainer cnt = new MapContainer(HTML_API_KEY);
        
        Double LatitudeBP = li.getLatitude();
        Double LongitudeBP = li.getLongitude();
        
               System.out.println(li.getLatitude());
               System.out.println(li.getLatitude());
        
        Coord cord = new Coord(LatitudeBP, LongitudeBP);
        Style s = new Style();
        s.setFgColor(0xff0000);
        s.setBgTransparency(0);
        cnt.zoom(cord, 12);
        cnt.setShowMyLocation(true);
        cnt.setCameraPosition(cord);
        Button btnAddPath = new Button("Itinéraire");
        btnAddPath.setVisible(false);
        FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, Display.getInstance().convertToPixels(1));
        ///////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////
         f1 = new Form("Localisation");
        
        backmap = new Button("Back");
        
        
        
        
        f1.setLayout(new BorderLayout());

        if (current != null) {
            current.show();
            return;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////TEST HERE /////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////     
        ///////////////////////////////////////////////////////////////////////////////////////////////
        
        //Location position = LocationManager.getLocationManager().getCurrentLocationSync();
        //Check if location is turned on and your app is allowed to use it.
        System.out.println(position.getDirection());
        System.out.println(position.getLatitude());
        System.out.println(position.getLongitude());
        //////////////////////////////////////// Ajout Position Bonplan //////////////////////////////////
        Button btnAddMarker = new Button("Afficher Position");
         Button backmap = new Button("Back");
        btnAddMarker.addActionListener(e -> {

            btnAddPath.setVisible(true);

            cnt.setCameraPosition(new Coord(LatitudeBP, LongitudeBP));

            cnt.addMarker(
                    EncodedImage.createFromImage(markerImg, false),
                    cnt.getCameraPosition(),
                    li.getNom()+li.prenom,
                    "Addresse : " + li.getAdresse(),
                    evtt -> {
                        ToastBar.showMessage(li.getAdresse(), FontImage.MATERIAL_PLACE);
                    }
            );

        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////Afficher Position Bonplan Par Default/////////////////////////////////////////////
        cnt.setCameraPosition(new Coord(LatitudeBP, LongitudeBP));
        cnt.addMarker(
                EncodedImage.createFromImage(markerImg, false),
                cnt.getCameraPosition(),
                li.getNom()+li.prenom,
                "Addresse : " + li.getAdresse(),
                evtt -> {
                    ToastBar.showMessage(li.getAdresse(), FontImage.MATERIAL_PLACE);
                }
        );

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        Button btnMoveCamera = new Button("Move Camera");
        btnMoveCamera.addActionListener(e -> {
            cnt.setCameraPosition(new Coord(LongitudeBP, LatitudeBP));
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////Ajouter Path entre Localisation user et Bonplan/////////////////////////////////////
        btnAddPath.addActionListener(e -> {
            Coord src = new Coord(position.getLatitude(), position.getLongitude());
            Coord dest = cord;

            String encoded = getRoutesEncoded(src, dest);
            System.out.println("decode:" + encoded);
            Coord[] coords = decode(encoded);
            cnt.addPath(coords);
        });
     


        //////////////////////////////////////////////////////////////////////////////////////////////////////       
        ///////////////////////////////////Clear All//////////////////////////////////////////////
        Button btnClearAll = new Button("Clear All");
        btnClearAll.addActionListener(e -> {
            cnt.clearMapLayers();
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        Container root = LayeredLayout.encloseIn(
                BorderLayout.center(cnt),
                BorderLayout.south(
                        FlowLayout.encloseBottom(btnAddPath,btnAddMarker)
                        
                ),
                BorderLayout.north(backmap)
                
               
        );
        
        
        f1.add(BorderLayout.CENTER, root);
        
        f1.show();
               
               
               
               backmap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                userdresseursAffichage a=new userdresseursAffichage();
                a.getF().show();
            }
        });
               
              

            }
   });
          
     
              try {
                   enc = EncodedImage.create("/aa.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
              
               String image = URL+"images.jpg";
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
               
               Container con = new Container();
               lb=new SpanLabel("");
               lb.setText("Nom : "+"\n"+li.getNom()+"\n"+"Prenom : "+"\n"+ li.getPrenom());
               j.add(v);
               j.add(lb);
               
               
               
               
               o.add(details);
               o.add(btn_map);
                
                
                k.add(j);
                k.add(o);
             
               y.add(k);
               
               details.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               Form f2 = new Form (li.getNom()+" "+li.getPrenom());
               
              j1= new Container(BoxLayout.x());
              k1 = new Container(BoxLayout.y());
              o1 = new Container(BoxLayout.x());
                 backk = new Button("back");
                y1.add(backk);
        y1.getAllStyles().setBgImage(a);
                
                
            
              
              try {
                   enc = EncodedImage.create("/aa.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
              
               String image = URL+"01-1-775x517.jpg";
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v1 = new ImageViewer();
               v1.setImage(img);
               
               
               Container con = new Container();
               lb1=new SpanLabel("");
               lb1.setText("Mail : "+"\n"+li.getEmail_canonical()+"\n"+"Numéro : "+"\n"+ li.getNumtel()+"\n"+"Adresse"+li.getAdresse());
               j1.add(v1);
               j1.add(lb1);
               
               Calendar c = new Calendar();
               c.setDate(new Date());
                Date datejour = new Date();
                
                DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                datejourr = formatter.format(datejour);
                
                TextField message = new TextField("","Votre message");
                
                
                ComboBox cb = new ComboBox<String>("8h","9h","10h","11h","14h","15h","16h");
                
                
                
                prendrerdv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                CRUD cr = new CRUD();
                
               listerdv=cr.getallrdv();
                
                for (Rendez_vous r : listerdv) {
                
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                d = formatter.format(c.getDate());
                
                 heure = cb.getSelectedItem().toString();
            
              idd = r.getId()+1;
                

              // String datedujour = formatter.format(d1);

//              SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                Date f = sdf.parse(d);
//               System.out.println(datedujour.substring(5, 7));
                //System.out.println(heure);
//                System.out.println(c.getDate().toString().substring(0, 3));
                
                  if (r.getDate().equals(d) && r.getHeure().equals(heure) && r.getAccepteur_id()== li.getId() && r.getRemove()== 0 ){
                    
                     resultat = "non";
                     break;
                      
                }
                  
                  else if (li.getId() == LoggedUser.getId()){
                      
                      resultat = "nonnn";
                      break;
                  }
                  
                  else if (r.getDate().equals(d) && r.getHeure().equals(heure) && r.getDemandeur_id()== LoggedUser.getId() && r.getRemove()== 0){
                      
                      resultat = "no";
                       break;
                  
                  }else{
                      
                      resultat = "oui";
                  }
                  
                  
                  
            }
                
                Date daterendezvous = c.getDate();
                
                DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                daterendezvouss = formatter.format(daterendezvous);
               
                
                System.out.println(daterendezvouss.compareTo(datejourr));
                
                  if (c.getDate().toString().substring(0, 3).equals("Sun") || c.getDate().toString().substring(0, 3).equals("Sat")) {
                    Dialog.show("Echec", "Vous ne pouvez prendre un rendez-vous Samedi ou Dimanche", "ok", "");
                    
                }  else if(resultat == "non") {
                    Dialog.show("Echec", "cette date est  prise  ", "ok", "");
   
                } else if(resultat == "nonnn") {
                    Dialog.show("Echec", "vous ne pouvez pas prendre un rendez-vous avec vous-meme  ", "ok", "");
   
                }else if (daterendezvouss.compareTo(datejourr) < 0) {
                    Dialog.show("Echec", "cette date est passé ", "ok", "");
   
                }else if(resultat == "no") {
                    Dialog.show("Echec", "vous avez déja un rendez-vous avec ce date  ", "ok", "");
   
                } else if(resultat == "oui"){
                      
                        try{
                            s.db.execute("insert into rendez_vous(id,message,date,heure,demandeur_id,valide,accepteur_id,remove,service) values ('" + idd + "','" + message.getText() + "','" + d + "','" + heure + "'"
                                    + ",'" + LoggedUser.getId() + "','" + 0 + "','" + li.getId() + "','" + 0 + "','" + "Dressage" + "');");
                        System.out.println("insert ok");
                        Dialog.show("Confirmer", "votre ajout est affecte en sqllight", "ok", "");
                        }catch (IOException ex) {
                            System.out.println("error ajout");
                            
                        }
                        
                        Message  m = new Message("Vous avez un rendez a validé");

                               Display.getInstance().sendMessage(new String[] {li.getEmail_canonical()}, "Rendez-vous à validé", m);
                        
                              CRUD cc = new CRUD();
                              Rendez_vous r = new Rendez_vous(message.getText(), d, heure,LoggedUser.getId(),0,li.getId(),0,"Dressage");
                              Dialog.show("Confirmer", "votre ajout est affecte", "ok", "");
                               cc.ajoutrdv(r);
                               
                              
                              
                               Message  m1 = new Message("Votre rendez-vous a ete effectue avec succes");

                               Display.getInstance().sendMessage(new String[] {LoggedUser.getEmail()}, "Prise de rendez-vous", m1);
                               
                               
                }              
                               
                  
                  
                  
                  
                  
        }
                });
               
           
                
                
                
                
                
               o1.add(c);
               
               
                
                
                k1.add(j1);
                k1.add(o1);
               
               y1.add(k1);
               y1.add(cb);
               y1.add(message);
               y1.add(prendrerdv);
               
               
               f2.add(y1);
               
               
               
               
               
               
               
               
               
               
               
               
               
               backk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                userdresseursAffichage a=new userdresseursAffichage();
                a.getF().show();
            }
        });
               
               
               
               
        f2.show();
        
        
      
              
            
           }
         });
               
               
               
               
        }
           
           
           
           f.add(y);
               
        

           
           back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                CategorieAffichage a=new CategorieAffichage();
                a.getF().show();
            }
        });
        
           
          
          
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
        public static Coord[] decode(final String encodedPath) {
        int len = encodedPath.length();
        final ArrayList<Coord> path = new ArrayList<Coord>();
        int index = 0;
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int result = 1;
            int shift = 0;
            int b;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            result = 1;
            shift = 0;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            path.add(new Coord(lat * 1e-5, lng * 1e-5));
        }
        Coord[] p = new Coord[path.size()];
        for (int i = 0; i < path.size(); i++) {
            p[i] = path.get(i);
        }

        return p;
    }
        
        private String getRoutesEncoded(Coord src, Coord dest) {
        String ret = "";
        try {

            ConnectionRequest request = new ConnectionRequest("https://maps.googleapis.com/maps/api/directions/json", false);
            request.addArgument("key", HTML_API_KEY);
            request.addArgument("origin", src.getLatitude() + "," + src.getLongitude());
            request.addArgument("destination", dest.getLatitude() + "," + dest.getLongitude());
            System.out.println(src.getLatitude() + "," + src.getLongitude());
            System.out.println(dest.getLatitude() + "," + dest.getLongitude());
            System.out.println(HTML_API_KEY);
            NetworkManager.getInstance().addToQueueAndWait(request);
            Map<String, Object> response = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(request.getResponseData()), "UTF-8"));
            if (response.get("routes") != null) {
                ArrayList routes = (ArrayList) response.get("routes");
                if (routes.size() > 0) {
                    ret = ((LinkedHashMap) ((LinkedHashMap) ((ArrayList) response.get("routes")).get(0)).get("overview_polyline")).get("points").toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }
    
}

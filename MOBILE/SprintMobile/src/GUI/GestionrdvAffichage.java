/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.Rendez_vous;
import Services.CRUD;
import com.codename1.components.ImageViewer;
import com.codename1.messaging.Message;

import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;

/**
 *
 * @author haythem
 */
public class GestionrdvAffichage {
    
    
    Form gestionrdv;
    TextField tnom;
    TextField tetat;
    Button btnajout,btnaff;
    Button btnprendrerdv,rdvattente,rdvconfirme,acceuill,rdvattentesuperuser,rdvconfirmesuperuser,rdvconfirmesuperusermoi;
    public Resources theme;
    public Resources res;
    
    Container c;
    
    
    

    public GestionrdvAffichage() {
        
        
        
      theme = UIManager.initFirstTheme("/theme");
        
        c = new Container(new FlowLayout(Component.CENTER)); 
        
        
        Image a = theme.getImage("signin-background.jpg");
        
        c.getAllStyles().setBgImage(a);
        
        
        
        
        
         gestionrdv = new Form("Gestion des rendez-vous", new FlowLayout(Component.CENTER));
        
         
         acceuill = new Button(" Acceuil de notre plateforme");
        btnprendrerdv = new Button("Prendre un rendez-vous");
        rdvattente = new Button("Mes rendez-vous en attente");
        rdvconfirme = new Button("Mes rendez-vous confirmés");
        rdvattentesuperuser = new Button("Mes rendez-vous à validé");
        rdvconfirmesuperuser = new Button("Mes rendez-vous");
        rdvconfirmesuperusermoi = new Button("Mes rendez-vous à moi");
        
        
        
        
        c.add(acceuill);
        c.add(btnprendrerdv);
        
        System.out.println("hhahahahahah   "+LoggedUser.getRole());
        
        if (LoggedUser.getRole().startsWith("Educateur") || LoggedUser.getRole().startsWith("Vétéri") || LoggedUser.getRole().startsWith("garde"))
        {
            
          
            c.add(rdvattentesuperuser);
            c.add(rdvconfirmesuperuser);
            c.add(rdvconfirmesuperusermoi);
        }
        
        if (LoggedUser.getRole().startsWith("utilisateur"))
        {
            c.add(rdvattente);
            c.add(rdvconfirme);
        }
        
        
        
      
        
        rdvattentesuperuser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                rdvpourvalider a = new rdvpourvalider();
                a.getF().show();
            }
        });
        
        rdvconfirmesuperuser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                rdvsuperuser a = new rdvsuperuser();
                a.getF().show();
            }
        });
        
        
        acceuill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                AccueilAffichage a = new AccueilAffichage(res);
                a.getF().show();
            }
        });
        
        
        btnprendrerdv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                CategorieAffichage a=new CategorieAffichage();
                a.getF().show();
                
      

            }
        });
        
        rdvattente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                rdvattenteuserAffichage a=new rdvattenteuserAffichage();
                a.getF().show();
            }
        });
        
 
        
        rdvconfirme.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                rdvuserAffichage a=new rdvuserAffichage();
                a.getF().show();
            }
        });
        
        rdvconfirmesuperusermoi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                rdvuserAffichage a=new rdvuserAffichage();
                a.getF().show();
            }
        });

        gestionrdv.add(c);
        
        gestionrdv.show();
        
    }
    
    
    

    public Form getF() {
        return gestionrdv;
    }

    public void setF(Form f) {
        this.gestionrdv = f;
    }

    public TextField getTnom() {
        return tnom;
    }

    public void setTnom(TextField tnom) {
        this.tnom = tnom;
    }
    
}

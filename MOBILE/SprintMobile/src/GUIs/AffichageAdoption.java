/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hamouda
 */
public class AffichageAdoption {
    
    Form f;
     Button btnsignaler,btnadopter;
    
   
    private EncodedImage enc ;
    
    SpanLabel lb;
    String URL ="http://localhost/Animaux/web/Picture/";
    Container y,j,k,o,u;
        private Resources theme;
    
    
    
    public AffichageAdoption() {
            theme = UIManager.initFirstTheme("/theme");
        
        
        
        
        Image a0 = theme.getImage("signin-background.jpg");
        
        
         f = new Form("Animaux a adopter");
        CRUDD c =new CRUDD();
        ArrayList<Annonce> lis=c.getallAnnonce1();
        y= new Container(BoxLayout.y());
        Button back = new Button("Back");
        
        
       back.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Home h = new Home();
              h.getHome();
            
           }
         });

        
       
        
          for (Annonce a : lis) {
               
               if(a.getUser_id()!=LoggedUser.getId()){
               
               
               j= new Container(BoxLayout.x());
              k = new Container(BoxLayout.y());
              o = new Container(BoxLayout.x());
              
                
              btnadopter = new Button("Adopter");
              btnsignaler=new Button("Signaler");
              
               btnsignaler.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               Form f2 = new Form ("Signalement");
               
               Container y= new Container(BoxLayout.y());
        
        ComboBox cb = new ComboBox<>("Contenu inaproprie","Informations incorrectes","ce n'est pas interessant","Spam","Autres");
        
       Button b = new Button ("Signaler");
        
        y.add(cb);
        y.add(b);
        
        f2.add(y);
        f2.show();
        
        
            
           }
         });
               
                btnadopter.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Form f3 = new Form ("Je veux adopter");
               
               Container y= new Container(BoxLayout.y());
               
                SpanLabel sp1 = new SpanLabel("Pourquoi veut tu adopter cet animal ?");
               TextField t = new TextField();
                  SpanLabel sp2 = new SpanLabel("Est ce que vous avez d'autres animaux a la maison?");
                  TextField t2 = new TextField();
                 SpanLabel sp3 = new SpanLabel("Comment nous garantir que vous allez bien vous occupez de cet animal ?");
                  TextField t3 = new TextField();
                  SpanLabel sp4 = new SpanLabel("Veuillez Laisser nous votre numero s'il vous plait pour vous contacter");
                  TextField t4 = new TextField();
                  
                  
                   Button b = new Button("Envoyer demande d'adoption");
                   
                   y.add(sp1);
                   y.add(t);
                    y.add(sp2);
                   y.add(t2);
                    y.add(sp3);
                   y.add(t3);
                    y.add(sp4);
                   y.add(t4);
                   y.add(b);
               
               
        
      
        
        f3.add(y);
        f3.show();
      
            
           }
         });
                      
                      
               
               try {
                   enc = EncodedImage.create("/img.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
               String image = URL+a.getImage();
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
               
               Container con = new Container();
               lb=new SpanLabel("");
               lb.setText( "C'est un(e) "+a.getAnimal()+ " de race "+ a.getRace()+ " il est : "+ a.getDescription());
               j.add(v);
               j.add(lb);
               
           
               o.add(btnadopter);
                o.add(btnsignaler);
                
                k.add(j);
                k.add(o);
             
               y.add(k);
        }}
           Container con = new Container(BoxLayout.y());
              con.add(back);
              con.add(y);
             con.getAllStyles().setBgImage(a0);
           f.add(con);
           
        
        
    
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hamouda
 */
public class MesAnnonces {
    
     Button supprimer,modifier;
    ComboBox<String> cb ;
    Button b ;
    
   
    private EncodedImage enc ;
    Form f;
    Form f2;
    SpanLabel lb;
    String URL ="http://localhost/Animaux/web/Picture/";
    Container y,j,k,o,u;
    
    public void MesAnnonces(){
        f = new Form("Mes Annonces");
        CRUDD c =new CRUDD();
        ArrayList<Annonce> lis=c.getallAnnoncee();
        y= new Container(BoxLayout.y());
        
       
        
           for (int i = 0; i < lis.size(); i++) {
               
               
               j= new Container(BoxLayout.x());
              k = new Container(BoxLayout.y());
              o = new Container(BoxLayout.x());
              
                
              supprimer = new Button("Supprimer");
              modifier=new Button("Modifier");
            
             
             
              try {
                   enc = EncodedImage.create("/cropper.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
               String image = URL+lis.get(i).getImage();
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
            
          
               
               
               Container con = new Container();
               lb=new SpanLabel("");
               lb.setText( "C'est mon/ma "+lis.get(i).getAnimal()+ " de race "+ lis.get(i).getRace()+ " il est : "+ lis.get(i).getDescription()+ " et son prix : "+ lis.get(i).getPrix()+"dt");
               j.add(v);
               j.add(lb);
               
           
               o.add(supprimer);
                o.add(modifier);
                
                
                k.add(j);
                k.add(o);
             
               y.add(k);
           
        }
           f.add(y);
           f.show();
           
           
            Toolbar tb = f.getToolbar();
        tb.addMaterialCommandToSideMenu("Profil", FontImage.MATERIAL_ACCESS_ALARM, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Profil p = new Profil();
                p.getF().show();
            
            }
        });
        tb.addMaterialCommandToSideMenu("Acceuil", FontImage.MATERIAL_FORMAT_CLEAR,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
             Home a = new Home ();
            a.getHome();

            }
        });

           
    
    }
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
}

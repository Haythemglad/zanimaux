/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import GUI.AccueilAffichage;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.OnOffSwitch;
import com.codename1.components.SpanLabel;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;

import GUIs.NewRec;
import com.codename1.ui.plaf.UIManager;

/**
 *
 * @author haythem
 */
public class Acceuil {

    Form home;
    Form home0;
    Form f, f2;
    SpanLabel lb;
    Button Vente,acceuill;
    Button Adoption;
    Button LostAndFound;
    Button HorsLigne;

    private Form current;
    private Resources theme;
    
    public Resources res;

    String URL = "http://localhost/Animaux/web/Picture/";
    private EncodedImage enc;

    public Acceuil() {

        
        theme = UIManager.initFirstTheme("/theme");
        
        Image a = theme.getImage("signin-background.jpg");
        
        NewRec nn = new NewRec();

        f = new Form();
        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        {

            {
                {
                    
                    
                    home = new Form("Accueil");
                    
                    acceuill = new Button("acceuil de notre plateforme");

                    

                    HorsLigne = new Button("Ma boite privée");

                    try {
                        enc = EncodedImage.create("/img.jpg").scaledEncoded(150, 150);

                    } catch (IOException e) {
                        System.out.println("Mehdi");

                    }
                    String image = URL + "slide03.jpg";
                    Image img = URLImage.createToStorage(enc, image, image, URLImage.RESIZE_SCALE);

                    ImageViewer v = new ImageViewer();
                    v.setImage(img);

                    String image2 = URL + "slide02.jpg";
                    Image imgg = URLImage.createToStorage(enc, image2, image2, URLImage.RESIZE_SCALE);

                    ImageViewer vv = new ImageViewer();
                    vv.setImage(imgg);

                    String image3 = URL + "slide01.jpg";
                    Image imggg = URLImage.createToStorage(enc, image3, image3, URLImage.RESIZE_SCALE);

                    ImageViewer vvv = new ImageViewer();
                    vvv.setImage(imggg);

                    Container y = new Container(BoxLayout.x());
                    Container x = new Container(BoxLayout.x());
                    Container z = new Container(BoxLayout.x());
                    Container o = new Container(BoxLayout.y());
                    Container hl = new Container(BoxLayout.y());
                    
                    Vente = new Button("Mes réclamations",img);

                    Adoption = new Button("Créer une réclamation",imgg);

                    LostAndFound = new Button("Liste des utilisateurs",imggg);

                    o.getAllStyles().setBgImage(a);
                    
                   o.add(acceuill);
                    y.add(Vente);
                    
                    x.add(Adoption);
                    
                    z.add(LostAndFound);
                    
                    hl.add(HorsLigne);

                    o.add(y);
                    o.add(x);
                    o.add(z);
                    o.add(hl);

                    home.add(o);
                    home.show();
                    
                    
                    acceuill.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            AccueilAffichage a = new AccueilAffichage(res);
                            a.getF().show();

                        }
                    });

                    Vente.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            AffichageRec a = new AffichageRec();
                            a.getF().show();

                        }
                    });
                    Adoption.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            NewRec a = new NewRec();
                            a.getF().show();

                        }
                    });

                    HorsLigne.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            Form fff = new Form("Mes Réclamations personnalisés");
                            
                             Button back = new Button("Back");

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                Acceuil h = new Acceuil();
                h.getHome();

            }
        });
        fff.add(back);
                            try {

                                Cursor cursor = nn.db.executeQuery("select * from reclamation");
                                while (cursor.next()) {
                                    Container jj = new Container(BoxLayout.y());
                                    Container pere = new Container(BoxLayout.y());
                                    Container poichiche = new Container(BoxLayout.y());
                                    Row r = cursor.getRow();
                                    String titre = r.getString(0);
                                    String categorie = r.getString(1);
                                    String description = r.getString(2);
                                    
                                    

                                    Label l = new Label("Titre: " + titre);
                                    Label l2 = new Label("Catégorie: " + categorie);
                                    Label l3 = new Label("Description: " + description);

                                    Button SupprimerBtn = new Button("Supprimer");
                                    pere.add(l);
                                    pere.add(l2);
                                    pere.add(l3);
                                    poichiche.add(pere);
                                    poichiche.add(SupprimerBtn);

                                    jj.add(poichiche);

                                    fff.add(jj);
                                    fff.add("__________________________________");

                                    SupprimerBtn.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent evt) {

                                            try {
                                                nn.db.execute("DELETE  FROM reclamation WHERE Titre='" + titre + "'");

                                       //         Dialog.show("Reclamation supprimé avec succès",null);
                                                fff.removeComponent(jj);

                                            } catch (IOException ex) {
                                                System.out.println(ex);
                                            }

                                        }
                                    }
                                    );

                                }
                            } catch (IOException ex) {
                                System.out.println(ex);
                            }

                            fff.show();

                        }
                    });

                    LostAndFound.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            AffichageUser a = new AffichageUser();
                            a.getF().show();

                        }
                    });

                    

                    home.show();

                }

            }
        };

    }

  

    public Form getA() {
        return home;
    }

    public Form getHome() {
        return home;
    }

    public void setHome(Form home) {
        this.home = home;
    }

 
    

   

    public void setF(Form f) {
        this.home = f;
    }

}

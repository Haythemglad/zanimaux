/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Reclamation;
import Servicess.ServiceRec;
import com.codename1.components.Ads;
import com.codename1.components.SliderBridge;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.Layout;
import GUIs.Acceuil;

import java.util.ArrayList;

/**
 *
 * @author mehdi
 */
public class AffichageRec {

    Form f, f2;
    SpanLabel lb;

    public AffichageRec() {

        f = new Form("Mes Reclamations", BoxLayout.y());
        lb = new SpanLabel("");
        f.add(lb);

        Ads ads = new Ads("ca-app-pub-8437115912718506/3995894729", true);
        f.add(ads);

        Button back = new Button("Back");

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                Acceuil h = new Acceuil();
                h.getHome();

            }
        });
        f.add(back);

        ServiceRec s = new ServiceRec();
        ArrayList<Reclamation> lis = s.getallReclamation();
        for (Reclamation t : lis) {
            Container c = new Container(BoxLayout.y());
            Label l = new Label("Titre: " + t.getTitre());

            c.add(l);

            Button b = new Button();
            f2 = new Form("Ma Reclamation ", BoxLayout.y());
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    f2.removeAll();

                    Container c2 = new Container(BoxLayout.y());

                    Button back = new Button("Back");

                    back.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            AffichageRec h = new AffichageRec();
                            h.getF().show();

                        }
                    });
                    f2.add(back);
                    
                    Label l = new Label("Titre: " + t.getTitre());
                    Label l1 = new Label("Catégorie: " + t.getCategorie());

                    Label l3 = new Label("Description: " + t.getDescription());

                    Label l5 = new Label("Réponse: " + t.getReponse());

                    c2.add(l);
                    c2.add(l1);

                    c2.add(l3);

                    c2.add(l5);

                    f2.add(c2);
                    f2.show();
                }

            });

            ////////////////
            c.setLeadComponent(b);
            f.add(c);

        }

        
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}

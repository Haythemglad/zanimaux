/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hamouda
 */
public class Profil {
    
    Form f ;
    ComboBox<String> cc;
    
     public Button Affichage;
   public Button AjoutAnnonce;
    Button supprimer,modifier;
    ComboBox<String> cb ;
    Button b ;
     private Resources theme;
    
   
    private EncodedImage enc ;
   
    Form f2;
    SpanLabel lb;
    String URL ="http://localhost/Animaux/web/Picture/";
    Container y,j,k,o,u;
  

    public Profil() {
        
          theme = UIManager.initFirstTheme("/theme");
        
        
        Image a0 = theme.getImage("signin-background.jpg");
        
        Form f = new Form("Profil", new FlowLayout(Component.CENTER));
        
        
        Affichage = new Button ("Afficher mes annonces");
      
        AjoutAnnonce = new Button ("Ajouter une annonce");
        
       Button Acceuil = new Button("Page d'Acceuil ");
       
        Container w = new Container(new FlowLayout((Component.CENTER)));
         
       w.add(Acceuil);
         w.add(Affichage);
       w.add(AjoutAnnonce);
        
         f.add(w);
         w.getAllStyles().setBgImage(a0);
        f.show();
        
         Acceuil.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               Home h = new Home();
             h.getHome();
            
           }
         });
        
        
         Affichage.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
                f2 = new Form("Mes Annonces");
                
                 CRUDD c =new CRUDD();
                 
                 Button back = new Button("Back");
                 
                 
                 back.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              f.show();
            
           }
         });
        ArrayList<Annonce> lis=c.getallAnnoncee();
        y= new Container(BoxLayout.y());
        
        
       
          
        
       
        
           for (Annonce a : lis) {
               
               
               j= new Container(BoxLayout.x());
              k = new Container(BoxLayout.y());
              o = new Container(BoxLayout.x());
              
                modifier=new Button("Modifier");
              supprimer = new Button("Supprimer");
              
               supprimer.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
                CRUDD c = new CRUDD();
              
               
           c.annuler_annonce(a);
             Dialog.show("Annonce supprime avec succes", "favori", "ok", null);
             
             
               
           f.show();
                
            
           }
         });

              
               modifier.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
                Form f4 = new Form ("Modifier annonce");
               
               Container y= new Container(BoxLayout.y());
               
               SpanLabel sp0 = new SpanLabel("Categorie");
                cc = new ComboBox("Vente","Adoption","LostAndFound");
               
                SpanLabel sp1 = new SpanLabel("Animal");
                 TextField t = new TextField();
                  SpanLabel sp2 = new SpanLabel("Race");
                  TextField t2 = new TextField();
                 SpanLabel sp3 = new SpanLabel("Description");
                  TextField t3 = new TextField();
                  SpanLabel sp4 = new SpanLabel(" Prix");
                  TextField t4 = new TextField();
                  
                  
                   Button b = new Button("Modifier Annonce");
                   y.add(sp0);
                   y.add(cc);
                   y.add(sp1);
                   y.add(t);
                    y.add(sp2);
                   y.add(t2);
                    y.add(sp3);
                   y.add(t3);
                    y.add(sp4);
                   y.add(t4);
                   y.add(b);
                         Container con = new Container(BoxLayout.y());
              con.add(y);
              
             con.getAllStyles().setBgImage(a0);
                   

               
                    b.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               
               CRUDD c = new CRUDD();
               Annonce aa = new Annonce(a.getId(),cc.getSelectedItem(),t.getText(),t2.getText(),t3.getText(),Float.parseFloat(t4.getText()));
               
               
               if (aa.getPrix()<0){
             Dialog.show("Echec", "Vous ne pouvez pas saisir un prix negatif  ", "ok", "");}
               else{
           c.modifier_annonce(aa);
             Dialog.show("Annonce modifier avec succes", "favori", "ok", null);
             
           f.show();}
           
         
           }
         });
              
        
        f4.add(con);
        f4.show();
           }
         });
            
             
             
              try {
                   enc = EncodedImage.create("/cropper.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
               String image = URL+a.getImage();
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
            
          
               
               
               Container con = new Container();
               lb=new SpanLabel("");
               lb.setText( "C'est mon/ma "+a.getAnimal()+ " de race "+ a.getRace()+ " il est : "+ a.getDescription()+ " et son prix : "+ a.getPrix()+"dt");
               j.add(v);
               j.add(lb);
               
           
               o.add(supprimer);
                o.add(modifier);
                
                
                k.add(j);
                k.add(o);
             
               y.add(k);
           
        }
            Container con = new Container(BoxLayout.y());
              con.add(back);
              con.add(y);
             con.getAllStyles().setBgImage(a0);
           f2.add(con);
           
                f2.show();
           }});
         
         AjoutAnnonce.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
                Form f3 = new Form ("Ajout annonce");
               
               Container y= new Container(BoxLayout.y());
               
               SpanLabel sp0 = new SpanLabel("Categorie");
                cc = new ComboBox("Vente","Adoption","LostAndFound");
               
                SpanLabel sp1 = new SpanLabel("Animal");
                 TextField t = new TextField();
                  SpanLabel sp2 = new SpanLabel("Race");
                  TextField t2 = new TextField();
                 SpanLabel sp3 = new SpanLabel("Description");
                  TextField t3 = new TextField();
                  SpanLabel sp4 = new SpanLabel(" Prix");
                  TextField t4 = new TextField();
                  
                  
                   Button b = new Button("Ajouter annonce");
                   y.add(sp0);
                   y.add(cc);
                   y.add(sp1);
                   y.add(t);
                    y.add(sp2);
                   y.add(t2);
                    y.add(sp3);
                   y.add(t3);
                    y.add(sp4);
                   y.add(t4);
                   y.add(b);
                   Container con = new Container(BoxLayout.y());
              con.add(y);
              
             con.getAllStyles().setBgImage(a0);
           
                   

               
                    b.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               
               CRUDD c = new CRUDD();
               Annonce a = new Annonce(cc.getSelectedItem(),t.getText(),t2.getText(),t3.getText(),Float.parseFloat(t4.getText()),"02.jpg");
               
           
           if (a.getPrix()<0){
             Dialog.show("Echec", "Vous ne pouvez pas saisir un prix negatif  ", "ok", "");}
           else{
               c.ajout_annonce(a);
            Dialog.show("Annonce Ajouté aux favoris avec succes", "favori", "ok", null);
           f.show();
           
           }
           }
         });
              
        
        f3.add(con);
        f3.show();
               
               
               
            
            
           }
         });
        
        
       
     
        
        
        
        
        
        
       
        
    }
    
    

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
}

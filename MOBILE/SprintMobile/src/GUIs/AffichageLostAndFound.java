/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hamouda
 */
public class AffichageLostAndFound {
    
   
     Form f;
     Button btnsignaler,btnMine,btnLocalsiser;
    
   
    private EncodedImage enc ;
    
    SpanLabel lb;
    String URL ="http://localhost/Animaux/web/Picture/";
    Container y,j,k,o,u;
        private Resources theme;
    
    
    
    
    public AffichageLostAndFound(){
        
            theme = UIManager.initFirstTheme("/theme");
        
        
        
        
        Image a0 = theme.getImage("signin-background.jpg");
        
        
         f = new Form("Animaux Perdu");
        CRUDD c =new CRUDD();
        ArrayList<Annonce> lis=c.getallAnnonce2();
        y= new Container(BoxLayout.y());
        Button back = new Button("Back");
        
        
       back.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Home h = new Home();
              h.getHome();
            
           }
         });

        
       
        
           for (Annonce a : lis) {
               
               if(a.getUser_id()!=LoggedUser.getId()){
               
               
               j= new Container(BoxLayout.x());
              k = new Container(BoxLayout.y());
              o = new Container(BoxLayout.x());
              
                
              btnMine = new Button("Its Mine");
              btnsignaler=new Button("Signaler");
              btnLocalsiser = new Button("Localiser");
              
          btnLocalsiser.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
              System.out.println("aaaa");
             // new MapAnnonce().Map();
               
               Form hi = new Form("Google Direction Service");
        
        final GoogleMap map = new GoogleMap();
       hi.setLayout(new BorderLayout());
        
        final TextField start = new TextField("Tunis");
      start.setHint("Start location");
       
       
        final TextField end = new TextField();
        end.setHint("Tunis,Tunisia");
        
      Container form = new Container();
        form.setLayout(new BorderLayout());
        form.addComponent(BorderLayout.NORTH, start);
      form.addComponent(BorderLayout.SOUTH, end);
       
       ActionListener routeListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
               if ( !"".equals(start.getText()) && !"".equals(end.getText())){
                    DirectionsRequest req = new DirectionsRequest();
                    req.setTravelMode(DirectionsRequest.TRAVEL_MODE_DRIVING);
                    req.setOriginName(start.getText());
                    req.setDestinationName("Tunis,Tunisia");
                    map.route(req, new DirectionsRouteListener(){
                        public void routeCalculated(DirectionsResult result) {
                            System.out.println("Successfully mapped route");
                        }
                        
                    });
                }
            }
            
        };
       
       start.addActionListener(routeListener);
        end.addActionListener(routeListener);
       
        hi.addComponent(BorderLayout.NORTH, form);
        
       hi.addComponent(BorderLayout.CENTER, map);
       hi.show();
            }
   });
  
  
  
  
              
               btnsignaler.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Form f2 = new Form ("Signalement");
               
               Container y= new Container(BoxLayout.y());
        
       ComboBox cb = new ComboBox<>("Contenu inaproprie","Informations incorrectes","ce n'est pas interessant","Spam","Autres");
        
       Button b = new Button ("Signaler");
        
        y.add(cb);
        y.add(b);
        
        f2.add(y);
        f2.show();
        
        
              
            
           }
         });
                      
                      btnMine.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Form f3 = new Form ("C'est le mien!!");
               
               Container y= new Container(BoxLayout.y());
               
               
               
               
               SpanLabel sp = new SpanLabel("OU vous avez perdu votre animal et comment?");
               ComboBox cb = new ComboBox("Especes","Cheques","Autres");
               SpanLabel sp2 = new SpanLabel("Comment on peut croire que c'est votre animal?");
               ComboBox cb1 = new ComboBox("4 jours", "1 semaine", "2 semaine", "3 semaine", "plus qu'in mois");
                SpanLabel sp3 = new SpanLabel("Qu'est ce qui caracterise le plus votre animal ?");
                TextField t = new TextField();
                  SpanLabel sp4 = new SpanLabel("Veuillez Laisser nous votre numero s'il vous plait pour vous contacter");
                  TextField t2 = new TextField();
                  Button b = new Button("Envoyer demande de recuperation");
               
               y.add(sp);
               y.add(cb);
               y.add(sp2);
               y.add(cb1);
               y.add(sp3);
               y.add(t);
               y.add(sp4);
               y.add(t2);
               y.add(b);
               
                       
               
        
      
        
        f3.add(y);
        f3.show();
        
      
              
            
           }
         });
                      
              
              
              
               
               try {
                   enc = EncodedImage.create("/img.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
               String image = URL+a.getImage();
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
               
               Container con = new Container();
               lb=new SpanLabel("");
               lb.setText( "C'est un(e) "+a.getAnimal()+ " de race "+ a.getRace()+ " il est : "+ a.getDescription());
               j.add(v);
               j.add(lb);
               
           
               o.add(btnMine);
                o.add(btnsignaler);
                o.add(btnLocalsiser);
                
                k.add(j);
                k.add(o);
             
               y.add(k);
        }}
             Container con = new Container(BoxLayout.y());
              con.add(back);
              con.add(y);
             con.getAllStyles().setBgImage(a0);
           f.add(con);
           
           
        
         
    
    
        
    
    
    
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
}

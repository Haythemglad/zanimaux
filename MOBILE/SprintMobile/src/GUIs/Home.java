/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIs;

import Entitys.Annonce;
import GUI.AccueilAffichage;
import Servicess.CRUDD;
import com.codename1.components.ImageViewer;
import com.codename1.components.OnOffSwitch;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;

/**
 *
 * @author haythem
 */
public class Home {
    
   public Form home;
   public Form home0;
   
   public Button Vente;
   public Button Adoption;
   public Button LostAndFound;
   
   public Resources res;
    
  public  String URL ="http://localhost/Animaux/web/Picture/";
     private EncodedImage enc ;
        private Resources theme;

    public Home() {
        
           theme = UIManager.initFirstTheme("/theme");
        
        
        
        
        Image a0 = theme.getImage("signin-background.jpg");
        

       home = new Form("Acceuil");
       
       Button Accueilll = new Button("Accueil de notre plateforme");
       
          Button profil = new Button("Profil");
          
          Accueilll.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
             AccueilAffichage a = new AccueilAffichage(res);
                a.getF().show();
            
           }
         });
                     
                      profil.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
              Profil p = new Profil();
              p.getF();
            
           }
         });
       
       
       Vente = new Button("_Animaux a vendre");
       
        Adoption = new Button("Animaux a adopter");
        
         LostAndFound = new Button("__Animaux Perdu");
         
          try {
                   enc = EncodedImage.create("/img.jpg").scaledEncoded(150, 150);
                   
               }
               catch ( IOException e) {
                   System.out.println("hamouda");
               
               }
               String image = URL+"slide03.jpg";
               Image img = URLImage.createToStorage(enc, image,image, URLImage.RESIZE_SCALE);
               
               ImageViewer v = new ImageViewer();
               v.setImage(img);
               
                String image2 = URL+"slide02.jpg";
               Image imgg = URLImage.createToStorage(enc, image2,image2, URLImage.RESIZE_SCALE);
               
               ImageViewer vv = new ImageViewer();
               vv.setImage(imgg);
               
               String image3 = URL+"slide01.jpg";
               Image imggg = URLImage.createToStorage(enc, image3,image3, URLImage.RESIZE_SCALE);
               
               ImageViewer vvv = new ImageViewer();
               vvv.setImage(imggg);
         
         
         
        Container y = new Container(BoxLayout.x());
        Container x = new Container(BoxLayout.x());
        Container z = new Container(BoxLayout.x());
        Container o = new Container(BoxLayout.y());
        
        
        y.add(Vente);
        y.add(v);
        x.add(Adoption);
        x.add(vv);
        z.add(LostAndFound);
        z.add(vvv);
        
        o.add(y);
        o.add(x);
        o.add(z);
        Container oo = new Container(BoxLayout.y());
        oo.add(Accueilll);
        oo.add(profil);
        oo.add(o);
        oo.getAllStyles().setBgImage(a0);
        home.add(oo);
        home.show();
        
         Vente.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               Affichage a = new Affichage();
               a.getF().show();
            
           }
         });
         Adoption.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               AffichageAdoption a = new AffichageAdoption();
               a.getF().show();
            
           }
         });
         
         LostAndFound.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               
               AffichageLostAndFound a = new AffichageLostAndFound();
               a.getF().show();
            
           }
         });
         
        
       
       home.show();
    }
    public Form getHome() {
        return home;
    }

    public void setHome(Form home) {
        this.home = home;
    }

    
   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Rendez_vous;
import com.mycompagny.Entite.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author haythem
 */
public class CRUD {
    
        public ArrayList<Rendez_vous> getrdvattenteuser() {
        ArrayList<Rendez_vous> listrdv = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficherrdvattenteuser/"+LoggedUser.getId()+"");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listRendezVous");
                    for (Map<String, Object> obj : list) {
                        Rendez_vous r = new Rendez_vous();
                        
                        float id = Float.parseFloat(obj.get("id").toString());
                        r.setId((int) id);
                        
                        r.setMessage(obj.get("message").toString());
                        r.setDate(obj.get("date").toString());
                        r.setHeure(obj.get("heure").toString());
                        
                        float demandeur_id = Float.parseFloat(obj.get("demandeur_id").toString());
                        r.setDemandeur_id((int) demandeur_id);
                        
                        r.setEmail_demandeur(obj.get("demandeur_mail").toString());
                        r.setEmail_accepteur(obj.get("accepteur_mail").toString());
                        
                        
                        float accepteur_id = Float.parseFloat(obj.get("accepteur_id").toString());
                        r.setAccepteur_id((int) accepteur_id);
                        
                        r.setService(obj.get("service").toString());
                        
                        
                        listrdv.add(r);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listrdv;
    }
        
        public ArrayList<Rendez_vous> getrdvuser() {
        ArrayList<Rendez_vous> listrdv = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficherrdvuser/"+LoggedUser.getId()+"");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listRendezVous");
                    for (Map<String, Object> obj : list) {
                        Rendez_vous r = new Rendez_vous();
                        
                       float id = Float.parseFloat(obj.get("id").toString());
                        r.setId((int) id);
                        
                        r.setMessage(obj.get("message").toString());
                        r.setDate(obj.get("date").toString());
                        r.setHeure(obj.get("heure").toString());
                        float demandeur_id = Float.parseFloat(obj.get("demandeur_id").toString());
                        r.setDemandeur_id((int) demandeur_id);
                        
                        r.setEmail_demandeur(obj.get("demandeur_mail").toString());
                        r.setEmail_accepteur(obj.get("accepteur_mail").toString());
                        
                        float accepteur_id = Float.parseFloat(obj.get("accepteur_id").toString());
                        r.setAccepteur_id((int) accepteur_id);
                        r.setService(obj.get("service").toString());
                        
                        
                        listrdv.add(r);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listrdv;
    }
        
         public ArrayList<User> getdresseurs() {
        ArrayList<User> listdresseurs = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficherdresseur");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listeuser");
                    for (Map<String, Object> obj : list) {
                        User u = new User();
                        
                        float id = Float.parseFloat(obj.get("id").toString());
                        u.setId((int) id);
                        
                        u.setNom(obj.get("nom").toString());
                        u.setPrenom(obj.get("prenom").toString());
                        float numtel = Float.parseFloat(obj.get("numtel").toString());
                        u.setNumtel((int) numtel);
                        u.setEmail_canonical(obj.get("mail").toString());
                        u.setAdresse(obj.get("adresse").toString());
                        double longitude = Float.parseFloat(obj.get("longitude").toString());
                        u.setLongitude((double) longitude);
                        double latitude = Float.parseFloat(obj.get("latitude").toString());
                        u.setLatitude((double) latitude);
                        
                        
                        listdresseurs.add(u);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listdresseurs;
    }
         
         public ArrayList<User> getveterinaires() {
        ArrayList<User> listveterinaires = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficheveterinaire");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listeuser");
                    for (Map<String, Object> obj : list) {
                        User u = new User();
                        
                     float id = Float.parseFloat(obj.get("id").toString());
                        u.setId((int) id);
                        
                        u.setNom(obj.get("nom").toString());
                        u.setPrenom(obj.get("prenom").toString());
                        float numtel = Float.parseFloat(obj.get("numtel").toString());
                        u.setNumtel((int) numtel);
                        u.setEmail_canonical(obj.get("mail").toString());
                        u.setAdresse(obj.get("adresse").toString());
                        double longitude = Float.parseFloat(obj.get("longitude").toString());
                        u.setLongitude((double) longitude);
                        double latitude = Float.parseFloat(obj.get("latitude").toString());
                        u.setLatitude((double) latitude);
                        
                        
                        listveterinaires.add(u);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listveterinaires;
    }
         
         public ArrayList<User> getgardeanimaux() {
        ArrayList<User> listgardeanimaux = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Affichegardeanimaux");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listeuser");
                    for (Map<String, Object> obj : list) {
                        User u = new User();
                        
                     float id = Float.parseFloat(obj.get("id").toString());
                        u.setId((int) id);
                        
                        u.setNom(obj.get("nom").toString());
                        u.setPrenom(obj.get("prenom").toString());
                        float numtel = Float.parseFloat(obj.get("numtel").toString());
                        u.setNumtel((int) numtel);
                        u.setEmail_canonical(obj.get("mail").toString());
                        u.setAdresse(obj.get("adresse").toString());
                        double longitude = Float.parseFloat(obj.get("longitude").toString());
                        u.setLongitude((double) longitude);
                        double latitude = Float.parseFloat(obj.get("latitude").toString());
                        u.setLatitude((double) latitude);
                        
                        
                        listgardeanimaux.add(u);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listgardeanimaux;
    }
         
         public ArrayList<Rendez_vous> getallrdv() {
        ArrayList<Rendez_vous> listrdv = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/afficherallrdv");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listRendezVous");
                    for (Map<String, Object> obj : list) {
                        Rendez_vous r = new Rendez_vous();
                        
                        float id = Float.parseFloat(obj.get("id").toString());
                        r.setId((int) id);
                        
                        r.setMessage(obj.get("message").toString());
                        r.setDate(obj.get("date").toString());
                        r.setHeure(obj.get("heure").toString());
                        float demandeur_id = Float.parseFloat(obj.get("demandeur_id").toString());
                        r.setDemandeur_id((int) demandeur_id);
                        
                        float accepteur_id = Float.parseFloat(obj.get("accepteur_id").toString());
                        r.setAccepteur_id((int) accepteur_id);
                        r.setService(obj.get("service").toString());
                        
                        
                        listrdv.add(r);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listrdv;
    }
         
         
         public void ajoutrdv (Rendez_vous r){
         ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/RDV/ajoutrdv/"+r.getDemandeur_id()+"/"+r.getAccepteur_id()+"?&message=" + r.getMessage()+"&date="+ r.getDate()+"&heure="+ r.getHeure()+"&valide="+ r.getValide()+ "&remove=" + r.getRemove()+ "&service=" +r.getService();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
         
         public void modifierrdv (Rendez_vous r){
         ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/RDV/modifierrdv/"+r.getId()+"?&date="+ r.getDate()+"&heure="+ r.getHeure();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
         public void annulerrdv (Rendez_vous r){
             ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/RDV/annulerrdv/"+r.getId()+"?&remove="+1;
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
         public void validerrdv (Rendez_vous r){
             ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/RDV/validerrdv/"+r.getId()+"?&valide="+1;
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
       public ArrayList<Rendez_vous> getrdvattentesuperuser() {
        ArrayList<Rendez_vous> listrdv = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficherrdvattentesuperuser/"+LoggedUser.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listRendezVous");
                    for (Map<String, Object> obj : list) {
                        Rendez_vous r = new Rendez_vous();
                        
                        float id = Float.parseFloat(obj.get("id").toString());
                        r.setId((int) id);
                        
                        r.setMessage(obj.get("message").toString());
                        r.setDate(obj.get("date").toString());
                        r.setHeure(obj.get("heure").toString());
                        
                        float demandeur_id = Float.parseFloat(obj.get("demandeur_id").toString());
                        r.setDemandeur_id((int) demandeur_id);
                        
                        r.setEmail_demandeur(obj.get("demandeur_mail").toString());
                        r.setEmail_accepteur(obj.get("accepteur_mail").toString());
                        
                        float accepteur_id = Float.parseFloat(obj.get("accepteur_id").toString());
                        r.setAccepteur_id((int) accepteur_id);
                        
                        r.setService(obj.get("service").toString());
                        
                        
                        listrdv.add(r);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listrdv;
    }
        
        public ArrayList<Rendez_vous> getrdvsuperuser() {
        ArrayList<Rendez_vous> listrdv = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/RDV/Afficherrdvsuperuser/"+LoggedUser.getId()+"");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> rdvs = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(rdvs);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) rdvs.get("listRendezVous");
                    for (Map<String, Object> obj : list) {
                        Rendez_vous r = new Rendez_vous();
                        
                     //   float id = Float.parseFloat(obj.get("id").toString());
                     //   r.setId((int) id);
                        
                        r.setMessage(obj.get("message").toString());
                        r.setDate(obj.get("date").toString());
                        r.setHeure(obj.get("heure").toString());
                        float demandeur_id = Float.parseFloat(obj.get("demandeur_id").toString());
                        r.setDemandeur_id((int) demandeur_id);
                        
                        r.setEmail_demandeur(obj.get("demandeur_mail").toString());
                        r.setEmail_accepteur(obj.get("accepteur_mail").toString());
                        
                        float accepteur_id = Float.parseFloat(obj.get("accepteur_id").toString());
                        r.setAccepteur_id((int) accepteur_id);
                        r.setService(obj.get("service").toString());
                        
                        
                        listrdv.add(r);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listrdv;
    }  
        
        
        
        
        public void modifierrdv (User u){
         ConnectionRequest con = new ConnectionRequest();
          String Url = "http://localhost/animaux/web/app_dev.php/RDV/map/"+u.getId()+"?&long="+ u.getLongitude()+"&lat="+ u.getLatitude();
        con.setUrl(Url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
        
        
        
        
        
        
         
    
}

/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.codename1.uikit.cleanmodern;

import com.codename1.components.FloatingHint;
import com.codename1.io.Log;
import com.codename1.sms.twilio.TwilioSMS;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.mycompagny.Entite.Forum;
import com.mycompagny.Entite.User;
import com.mycompagny.Service.ForumService;

import com.mycompagny.Service.UserService;

/**
 * AddSujet UI
 *
 * @author Shai Almog
 */
public class AddSujet extends ForumBaseForm {

    public AddSujet( Resources res) {
         super("Newsfeed", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Profile");
        getContentPane().setScrollVisible(false);  
        super.addSideMenu(res);
        tb.addSearchCommand(e -> {});
       
        setUIID("Add");
                
        TextField Titre = new TextField("", "Titre", 20, TextField.ANY);
        TextField Tag = new TextField("", "Tag", 20, TextField.EMAILADDR);
        TextField Contenu = new TextField("", "Contenu", 20, TextField.PASSWORD);
      
        Titre.setSingleLineTextArea(false);
        Tag.setSingleLineTextArea(false);
        Contenu.setSingleLineTextArea(false);

        Button Ajouter = new Button("Ajouter Sujet");
        Ajouter.addActionListener((e) -> {
//            ServiceTask ser = new ServiceTask();
//            Task t = new Task(0, tnom.getText(), tetat.getText());
//            ser.ajoutTask(t);
			ForumService fs = new ForumService();
			Forum f = new Forum(Titre.getText(),Tag.getText(),Contenu.getText());
			fs.AjouterForum(f);

		}); 

   
        
        Container content = BoxLayout.encloseY(
                new Label("Ajouter Sujet", "LogoLabel"),
                new FloatingHint(Titre),
                createLineSeparator(),
                new FloatingHint(Tag),
                createLineSeparator(),
                new FloatingHint(Contenu),
                createLineSeparator()
              
        );
        content.setScrollableY(true);
        add(BorderLayout.CENTER, content);
        add(BorderLayout.SOUTH, BoxLayout.encloseY(
                Ajouter
            
        ));
        Ajouter.requestFocus();

        Ajouter.addActionListener(e -> new AffichageForum(res).show());
    }
    
}

/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.codename1.uikit.cleanmodern;

import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Image;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.mycompagny.Entite.Categorie;
import com.mycompagny.Entite.Service;
import com.mycompagny.Service.ServiceAjoutservice;
import java.util.Date;
import jdk.nashorn.internal.objects.NativeDate;

public class AjoutService extends BaseForm {


    public AjoutService(Resources res) {
        super("Newsfeed", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Ajouter un service");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        add(LayeredLayout.encloseIn(
                sl
        ));

        TextField titre = new TextField();
        titre.setUIID("TextFieldBlack");
        addStringValue("Titre", titre);

        TextField des = new TextField();
        des.setUIID("TextFieldBlack");
        addStringValue("Description", des);

        TextField ad = new TextField();
        ad.setUIID("TextFieldBlack");
        addStringValue("Adresse", ad);

        TextField numtel = new TextField(TextField.NUMERIC);
        numtel.setUIID("TextFieldBlack");
        addStringValue("Numéro de téléphone", numtel);

        Picker p = new Picker();
        p.setStrings("Vétérinaires", "Education canine", "Garde pour animaux");

        addStringValue("Catégorie", p);
        Button ajouter = new Button("Ajouter");
        super.add(ajouter);
        ajouter.addActionListener(e -> {
            Service s = new Service();
            s.setTitle(titre.getText());
            s.setDescription(des.getText());
            s.setAdresseS(ad.getText());
            s.setImage("logo-2.png");
            s.setFavori(0);
            s.setNumtel(Integer.parseInt(numtel.getText()));
            s.setUserservice(SignInForm.LoggedUser.getUsername());
            //s.setCreateDt((Date)System.currentTimeMillis());

            s.setCatservice(p.getSelectedString());
            if (p.getSelectedString().equals("Vétérinaires")){
                s.setCategorie("Vétérinaires");
            }
            if (p.getSelectedString().equals("Education canine")){
                s.setCategorie("Education Canine");
            }
            if (p.getSelectedString().equals("Garde pour animaux")){
                s.setCategorie("Garde pour animaux");
            }
            
            ServiceAjoutservice aj = new ServiceAjoutservice();
//            Service s=new Service(titre.getText(),des.getText(),ad.getText(),Integer.parseInt(numtel.getText()),"seif");
            aj.ajoutService(s);
            new AffichageCategorie(res).show();
            System.out.println(s.getAdresseS());
        }
        );

    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}

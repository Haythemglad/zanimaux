/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.codename1.uikit.cleanmodern;

import com.mycompagny.Entite.SmsSender;
import com.codename1.components.FloatingHint;
import com.codename1.io.Log;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.sms.twilio.TwilioSMS;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.mycompagny.Entite.User;

import com.mycompagny.Service.UserService;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Random;


/**
 * Signup UI
 *
 * @author Shai Almog
 */
public class SignUpForm extends BaseForm {
public static User sup;
    public SignUpForm( Resources res) {
        super(new BorderLayout());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        tb.setUIID("Container");
        getTitleArea().setUIID("Container");
        Form previous = Display.getInstance().getCurrent();
        tb.setBackCommand("", e -> previous.showBack());
        setUIID("SignIn");
                
        TextField username = new TextField("", "Username", 20, TextField.ANY);
        TextField email = new TextField("", "E-Mail", 20, TextField.EMAILADDR);
        TextField password = new TextField("", "Password", 20, TextField.PASSWORD);
        TextField prenom = new TextField("", "prenom", 20, TextField.ANY);
        TextField nom = new TextField("", "nom", 20, TextField.ANY);
        TextField numtel = new TextField("", "numero de telephone", 20, TextField.ANY);
        
        username.setSingleLineTextArea(false);
        email.setSingleLineTextArea(false);
        password.setSingleLineTextArea(false);
        prenom.setSingleLineTextArea(false);
        nom.setSingleLineTextArea(false);
        numtel.setSingleLineTextArea(false);
        Button next = new Button("Next");
       Picker datePicker = new Picker();
datePicker.setType(Display.PICKER_TYPE_DATE);
           
           datePicker.setDate(new Date());
           
        next.addActionListener((e) -> {
//            ServiceTask ser = new ServiceTask();
//            Task t = new Task(0, tnom.getText(), tetat.getText());
//            ser.ajoutTask(t);
              Random r = new Random();
String val = "" + r.nextInt(10000);
while(val.length() < 4) {
    val = "0" + val;
    
}
int foo = Integer.parseInt(val);
      
            System.out.println(val);
			User a = new User(username.getText(),email.getText(),password.getText(),nom.getText(),prenom.getText(),Integer.parseInt(numtel.getText()),foo);
			
                        
			sup=a;
                         SmsSender msg = new SmsSender();  
            try {
                msg.send();
            } catch (URISyntaxException ex) {
             
            }

		});
        
        Button signIn = new Button("Sign In");
        signIn.addActionListener(e -> previous.showBack());
        signIn.setUIID("Link");
        Label alreadHaveAnAccount = new Label("Already have an account?");
        
        Container content = BoxLayout.encloseY(
                new Label("Sign Up", "LogoLabel"),
                new FloatingHint(username),
                createLineSeparator(),
                new FloatingHint(email),
                createLineSeparator(),
                new FloatingHint(password),
                createLineSeparator(),
                new FloatingHint(prenom),
                createLineSeparator(),
                new FloatingHint(nom),
                createLineSeparator(),
                new FloatingHint(numtel),
                createLineSeparator()
        );
        content.setScrollableY(true);
        add(BorderLayout.CENTER, content);
        add(BorderLayout.SOUTH, BoxLayout.encloseY(
                next,
                FlowLayout.encloseCenter(alreadHaveAnAccount, signIn)
        ));
        next.requestFocus();

        next.addActionListener(e -> new ActivateForm(res).show());
    }
 public int randomnumber()
{
              Random r = new Random();
String val = "" + r.nextInt(10000);
while(val.length() < 4) {
    val = "0" + val;




}
int foo = Integer.parseInt(val);
return foo;
}
}



 

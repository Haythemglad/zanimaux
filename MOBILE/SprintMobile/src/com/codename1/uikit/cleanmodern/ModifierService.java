/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codename1.uikit.cleanmodern;

import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.mycompagny.Entite.Service;
import com.mycompagny.Service.ServiceAjoutservice;
import com.mycompagny.Service.ServiceModifierService;

/**
 *
 * @author Nouha
 */
public class ModifierService extends BaseForm {

    public ModifierService(Resources res, Service ta) {
        super("Newsfeed", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Modifier le service");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        add(LayeredLayout.encloseIn(
                sl
        ));

        TextField titre = new TextField();
        titre.setUIID("TextFieldBlack");
        addStringValue("Titre", titre);

        TextField des = new TextField();
        des.setUIID("TextFieldBlack");
        addStringValue("Description", des);

        TextField ad = new TextField();
        ad.setUIID("TextFieldBlack");
        addStringValue("Adresse", ad);

        TextField numtel = new TextField(TextField.NUMERIC);
        numtel.setUIID("TextFieldBlack");
        addStringValue("Numéro de téléphone", numtel);

        Button modifier = new Button("Modifier");
        super.add(modifier);
        ServiceModifierService mod = new ServiceModifierService();
        titre.setText(ta.getTitle());
        des.setText(ta.getDescription());
        ad.setText(ta.getAdresseS());
        numtel.setText(String.valueOf(ta.getNumtel()));

        modifier.addActionListener(e -> {
            ta.setTitle(titre.getText());
            ta.setDescription(des.getText());
            ta.setAdresseS(ad.getText());
            ta.setNumtel(Integer.parseInt(numtel.getText()));
            mod.ModifierService(ta);
            new AffichageCategorie(res).show();

        }
        );

    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }

}

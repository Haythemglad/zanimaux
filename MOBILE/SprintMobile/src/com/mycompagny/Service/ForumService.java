/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompagny.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.DateFormat;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.events.ActionListener;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import com.mycompagny.Entite.Forum;
import com.mycompagny.Entite.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nouha
 */
public class ForumService {
    ConnectionRequest con = new ConnectionRequest();


    public void AjouterForum(Forum f) {

 

        String Url = "http://localhost/Animaux/web/app_dev.php/Forum/AddForum?titre="+f.getTitre()+"&auteur="+LoggedUser.username+"&blog="+f.getBlog()+"&tags="+f.getTags()+"";

        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

    }
    public ArrayList<Forum> getListService(String json) {

        ArrayList<Forum> listForum = new ArrayList<>();

        try {
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> Forums = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println(Forums);

            List<Map<String, Object>> list = (List<Map<String, Object>>) Forums.get("root");

            for (Map<String, Object> obj : list) {
                Forum c = new Forum();

                // System.out.println(obj.get("id"));
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                c.setId((int) id);
                //e.setId(Integer.parseInt(obj.get("id").toString().trim()));
                c.setTitre(obj.get("titre").toString());
                c.setBlog(obj.get("blog").toString());
//                String pattern = "dd/MM/yyyy";
//                DateFormat df = new SimpleDateFormat(pattern);
//                 String reportDate = df.format(obj.get("createDt"));
                //c.setCreateDt(obj.get("createDt").toString());
                c.setTags(obj.get("tags").toString());
              // c.setImage(obj.get("image").toString());
//                c.setCategorie(obj.get("categorie").toString());
                c.setAuteur(obj.get("auteur").toString());
                //c.setCreateDt(reportDate);
//                float cat = Float.parseFloat(obj.get("catservice").toString());
//                c.setCatservice((int) cat);
              

                System.out.println(c);
                listForum.add(c);

            }

        } catch (IOException ex) {
        }
        System.out.println(listForum);
        return listForum;

    }

    ArrayList<Forum> listForum = new ArrayList<>();

    public ArrayList<Forum> getList2() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/Forum/api");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ForumService ser = new ForumService();
                listForum = ser.getListService(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listForum;
    }
    
    

    //****************************************************************************************
    public ArrayList<Forum> getListmesSujet(String json) {

        ArrayList<Forum> listForum2 = new ArrayList<>();

        try {
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> Forums = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println(Forums);

            List<Map<String, Object>> list = (List<Map<String, Object>>) Forums.get("root");

            for (Map<String, Object> obj : list) {
                Forum c = new Forum();

                // System.out.println(obj.get("id"));
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                c.setId((int) id);
                //e.setId(Integer.parseInt(obj.get("id").toString().trim()));
                c.setTitre(obj.get("titre").toString());
                c.setBlog(obj.get("blog").toString());
//                String pattern = "dd/MM/yyyy";
//                DateFormat df = new SimpleDateFormat(pattern);
//                 String reportDate = df.format(obj.get("createDt"));
                //c.setCreateDt(obj.get("createDt").toString());
                c.setTags(obj.get("tags").toString());
      //  c.setImage(obj.get("image").toString());
//                c.setCategorie(obj.get("categorie").toString());
                c.setAuteur(obj.get("auteur").toString());
                //c.setCreateDt(reportDate);
//                float cat = Float.parseFloat(obj.get("catservice").toString());
//                c.setCatservice((int) cat);
              

                System.out.println(c);
                listForum2.add(c);

            }

        } catch (IOException ex) {
        }
        System.out.println(listForum2);
        return listForum2;

    }

    ArrayList<Forum> listForum2 = new ArrayList<>();

    public ArrayList<Forum> getList3(String auteur) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Animaux/web/app_dev.php/Forum/mine?auteur="+auteur+"");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ForumService ser = new ForumService();
                listForum2 = ser.getListmesSujet(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listForum2;
    }
      public void Supprimer(int id) {

 
        String Url = "http://localhost/Animaux/web/app_dev.php/Forum/deleteM?id="+id+"";

        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

    }
       public void ModifierForum(Forum f) {

 

        String Url = "http://localhost/Animaux/web/app_dev.php/Forum/Modifier?id="+f.getId()+"&titre="+f.getTitre()+"&blog="+f.getBlog()+"&tags="+f.getTags()+"";

        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

    }

}

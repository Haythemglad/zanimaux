/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompagny.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompagny.Entite.Service;
import com.mycompagny.Entite.User;
import static com.codename1.uikit.cleanmodern.SignInForm.LoggedUser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Nouha
 */
public class Servicemalistvet {
    public ArrayList<Service> getListService(String json) {

        ArrayList<Service> listservice = new ArrayList<>();

        try {
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> services = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println(services);

            List<Map<String, Object>> list = (List<Map<String, Object>>) services.get("root");

            for (Map<String, Object> obj : list) {
                Service c = new Service();

                // System.out.println(obj.get("id"));
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                c.setId((int) id);
                //e.setId(Integer.parseInt(obj.get("id").toString().trim()));
                c.setTitle(obj.get("title").toString());
                c.setDescription(obj.get("description").toString());
//                String pattern = "dd/MM/yyyy";
//                DateFormat df = new SimpleDateFormat(pattern);
//                 String reportDate = df.format(obj.get("createDt"));
                //c.setCreateDt(obj.get("createDt").toString());
                c.setAdresseS(obj.get("adresseS").toString());
                c.setImage(obj.get("image").toString());
//                c.setCategorie(obj.get("categorie").toString());
                c.setUserservice(obj.get("userservice").toString());
                //c.setCreateDt(reportDate);
//                float cat = Float.parseFloat(obj.get("catservice").toString());
//                c.setCatservice((int) cat);
                float numtel = Float.parseFloat(obj.get("numtel").toString());
                c.setNumtel((int) numtel);

                System.out.println(c);
                listservice.add(c);

            }

        } catch (IOException ex) {
        }
        System.out.println(listservice);
        return listservice;

    }

    ArrayList<Service> listservice = new ArrayList<>();

    public ArrayList<Service> getList2() {
       
        ConnectionRequest con = new ConnectionRequest();
        System.out.println("---------------------"+LoggedUser.getUsername());
        con.setUrl("http://localhost/animaux/web/app_dev.php/services/messervicesvetapi?userservice="+LoggedUser.getUsername());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Servicemalistvet ser = new Servicemalistvet();
                listservice = ser.getListService(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listservice;
    }

}

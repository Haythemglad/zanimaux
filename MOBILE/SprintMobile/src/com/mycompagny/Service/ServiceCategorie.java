/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompagny.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompagny.Entite.Categorie;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nouha
 */
public class ServiceCategorie {

    public ArrayList<Categorie> getListCategorie(String json) {

        ArrayList<Categorie> listcategorie = new ArrayList<>();

        try {
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> categories = j.parseJSON(new CharArrayReader(json.toCharArray()));
            System.out.println(categories);

            List<Map<String, Object>> list = (List<Map<String, Object>>) categories.get("root");

            for (Map<String, Object> obj : list) {
                Categorie c = new Categorie();

                // System.out.println(obj.get("id"));
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                c.setId((int) id);
                //e.setId(Integer.parseInt(obj.get("id").toString().trim()));
                c.setName(obj.get("name").toString());
                c.setImage(obj.get("image").toString());
                c.setUsercat(obj.get("usercat").toString());
                //c.setServices((ArrayList<Service>) obj.get("servicecat"));                
                //System.out.println(c.getServices());
                System.out.println(c);
                listcategorie.add(c);

            }

        } catch (IOException ex) {
        }
        System.out.println(listcategorie);
        return listcategorie;

    }

    ArrayList<Categorie> listcategorie = new ArrayList<>();

    public ArrayList<Categorie> getList2() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/animaux/web/app_dev.php/services/listecapi");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceCategorie ser = new ServiceCategorie();
                listcategorie = ser.getListCategorie(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listcategorie;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompagny.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.mycompagny.Entite.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author nessrine
 */
public class UserService {

    ConnectionRequest con = new ConnectionRequest();

    public void Signup(User a) {

        String pw_hash = BCrypt.hashpw(a.getPassword(), BCrypt.gensalt());

        String Url = "http://localhost/Animaux/web/app_dev.php/user/register?username=" + a.getUsername() + "&email=" + a.getEmail() + "&password=" + pw_hash + "&nom="+a.getNom()+"&prenom="+a.getPrenom()+"&numtel="+a.getNumtel()+"&Activationcode="+a.getActivationcode()+"";

        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

    }

    public boolean verifFailed(String s, String toVerif) {
        System.out.println(s.substring(1, s.length() - 1).equals("Failed"));
        return s.substring(1, s.length() - 1).equals(toVerif);
    }

    public User login(User a) {
          ArrayList<User> user = new ArrayList<>();
        String url = "http://localhost/Animaux/web/app_dev.php/user/log?username=" + a.getUsername() + "&password=" + a.getPassword() + "";
        con.setUrl(url);
        con.addResponseListener((e) -> {
            String stra = new String(con.getResponseData());
            System.out.println(stra); 
            
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
       String str = new String(con.getResponseData());
        System.out.println("aaaaaaaaaaaaaaaaaaaa"+str);
        if (!verifFailed(str, "Failed")) {
                JSONParser j = new JSONParser();
                Map<String, Object> obj;
                try {
                    obj = j.parseJSON(new CharArrayReader(str.toCharArray()));
                    User u = new User();
                    float id = Float.parseFloat(obj.get("id").toString());
                    u.setId((int) id);
                    u.setNom("" + obj.get("nom"));
                    u.setPrenom("" + obj.get("prenom"));
                    u.setEmail("" + obj.get("email"));
                    u.setUsername("" + obj.get("username"));
                    u.setPassword("" + obj.get("password"));
                    u.setRole("" + obj.get("role"));
                     user.add(u);
                     u.setCurrentUser(u);
                    System.out.println("usssssser id"+User.getCurrentUser().getId());
                    
                    System.out.println("-------------objet-----------------" + obj);
                         return user.get(0);
                } catch (IOException ex) {

                }
            }
         User u = new User();
        u.setId(-1);
        return u;

    }

    
      public User Signin(String json) throws IOException {

        ArrayList<User> user = new ArrayList<>();

        if (!verifFailed(json, "Failed")) {
            try {
                System.out.println(json);
                JSONParser j = new JSONParser();
                System.out.println("	uuuuuuuu");
                Map<String, Object> obj = j.parseJSON(new CharArrayReader(json.toCharArray()));
                System.out.println(obj);
                System.out.println("");
                //List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("root");
                User u = new User();
                System.out.println(obj.get("id"));
                float id = Float.parseFloat(obj.get("id").toString());
                System.out.println(id);
                u.setId((int) id);
                u.setNom("" + obj.get("nom"));
                u.setPrenom("" + obj.get("prenom"));
                u.setEmail("" + obj.get("email"));
                System.out.println(u);
                user.add(u);
                System.out.println(user);

                return user.get(0);
            } catch (IOException ex) {
            }
        }
        User u = new User();
        u.setId(-1);
        return u;
    }
 
}

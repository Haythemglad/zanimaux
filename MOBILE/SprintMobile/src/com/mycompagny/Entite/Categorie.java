/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompagny.Entite;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Nouha
 */
public class Categorie {

    private int id;
    private String name;
    private String image;
    private String usercat;
   

   

  
    //private ArrayList<Service>  services;

    /*public ArrayList<Service> getServices() {
        return services;
    }

    public void setServices(ArrayList<Service> services) {
        this.services = services;
    }*/

   


  
    public Categorie() {
    }

    public Categorie(int id, String name, String image, String usercat) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.usercat = usercat;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsercat() {
        return usercat;
    }

    public void setUsercat(String usercat) {
        this.usercat = usercat;
    }

    @Override
    public String toString() {
        return "Categorie{" + "id=" + id + ", name=" + name + ", image=" + image + ", usercat=" + usercat + '}';
    }

}
